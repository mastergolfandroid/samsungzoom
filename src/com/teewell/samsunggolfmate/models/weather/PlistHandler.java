package com.teewell.samsunggolfmate.models.weather;

import java.util.HashMap;
import java.util.Map;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**  
 * @Project GolfMate20130123
 * @package com.teewell.golfmate.utils
 * @title A.java 
 * @Description TODO
 * @author Administrator
 * @date 2013-1-28 下午4:52:20
 * @Copyright Copyright(C) 2013-1-28
 * @version 1.0.0
 */ 
public class PlistHandler extends DefaultHandler {  
    private boolean keyElementBegin = false;  
    private String key;  
    private boolean valueElementBegin = false;  
    private Map<String,String> value;  
    private static final String ENDSTRING = "市";
    public Map<String,String> getMapResult() {  
        return value;  
    }  
    
    @Override
	public void startDocument() throws SAXException {
		value = new HashMap<String, String>();
		super.startDocument();
	}

	@Override  
    public void startElement(String uri, String localName, String qName,  
            Attributes attributes) throws SAXException {      
        if ("key".equals(localName)) {  
            keyElementBegin = true;  
        }  
        if ("string".equals(localName)) {  
            valueElementBegin = true;  
        }  
    }  
    @Override  
    public void characters(char[] ch, int start, int length)  
            throws SAXException {  
        if (length > 0) {  
            if (keyElementBegin) {  
                key = new String(ch, start, length);  
            }  
            if (valueElementBegin) {  
                value.put(key, new String(ch, start, length)) ;
                if (!key.endsWith(ENDSTRING)) {
					key+=ENDSTRING;
					value.put(key, new String(ch, start, length)) ;
				}
            }  
        }  
    }  
  
    @Override  
    public void endElement(String uri, String localName, String qName)  
            throws SAXException {  
        if ("key".equals(localName)) {  
            keyElementBegin = false;  
        }  
        if ("string".equals(localName)) {  
            valueElementBegin = false;  
        }  
    }  
}

