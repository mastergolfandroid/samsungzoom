package com.teewell.samsunggolfmate.models.weather;

import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.ParseException;
import org.apache.http.util.EntityUtils;

import com.teewell.samsunggolfmate.beans.WeatherInfoBean;
import com.teewell.samsunggolfmate.utils.AsyncResponseListener;
import com.teewell.samsunggolfmate.views.CourseListCellWrapper;

import android.util.Log;

public class WeatherInfoResponseListener implements AsyncResponseListener{
	@SuppressWarnings("unused")
	private static final String TAG = WeatherInfoResponseListener.class.getSimpleName();
	private static final String TEMP_UNIT = "℃";
	private CourseListCellWrapper cellView;
	private String city;
	
	public WeatherInfoResponseListener(String cityString, CourseListCellWrapper cellView){
		this.cellView = cellView; // default type
		this.city = cityString;
	}
	
	@Override
	public void onResponseReceived(HttpEntity response) {
		// TODO Auto-generated method stub
    	try {
			String responseBody = EntityUtils.toString(response);
			WeatherInfoModel model = WeatherInfoModel.getInstance(null);
			model.submitWeatherInfo(city, responseBody);
			
			//change the cellview
			WeatherInfoBean bean = model.getWeatherInfoBean(city);
			if(null == bean){
				return;
			}
			cellView.wind.setText(bean.getWD());
			cellView.windLevel.setText(bean.getWS());
			cellView.temperature.setText(bean.getTemp()+TEMP_UNIT);
			
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}	
	}
	@Override
	public void onResponseReceived(Throwable response) {
		// TODO Auto-generated method stub
		
	}


}
