package com.teewell.samsunggolfmate.models.weather;

import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.ParseException;
import org.apache.http.util.EntityUtils;

import com.teewell.samsunggolfmate.beans.WeatherInfoBean;
import com.teewell.samsunggolfmate.utils.AsyncResponseListener;
import com.teewell.samsunggolfmate.views.CourseListCellWrapper;

public class WeatherImageResponseListener implements AsyncResponseListener{
	@SuppressWarnings("unused")
	private static final String TAG = WeatherImageResponseListener.class.getName();

	private CourseListCellWrapper cellView;
	private String city;
	
	public WeatherImageResponseListener(String cityString, CourseListCellWrapper cellView){
		this.cellView = cellView; // default type
		this.city = cityString;
	}
	
	@Override
	public void onResponseReceived(HttpEntity response) {
		// TODO Auto-generated method stub
    	try {
			String responseBody = EntityUtils.toString(response);
			WeatherInfoModel model = WeatherInfoModel.getInstance(null);
			model.submitWeatherImage(city, responseBody);
			
			//change the cellview
			WeatherInfoBean bean = model.getWeatherInfoBean(city);
			if(null == bean){
				return;
			}
			cellView.courseWeather.setImageBitmap(bean.getBitmap());
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			
//			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
//			e.printStackTrace();
		}	

	}
	@Override
	public void onResponseReceived(Throwable response) {
		// TODO Auto-generated method stub
		
	}


}
