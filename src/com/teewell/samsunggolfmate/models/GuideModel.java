package com.teewell.samsunggolfmate.models;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.HttpPost;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.util.Log;

import com.android.volley.Cache;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.ImageRequest;
import com.teewell.samsunggolfmate.beans.GuideBean;
import com.teewell.samsunggolfmate.common.Contexts;
import com.teewell.samsunggolfmate.costants.SharedPreferenceConstant;
import com.teewell.samsunggolfmate.utils.AbstractAsyncResponseListener;
import com.teewell.samsunggolfmate.utils.AsyncHttpClient;
import com.teewell.samsunggolfmate.utils.AsyncResponseListener;
import com.teewell.samsunggolfmate.utils.JsonProcessUtil;
import com.teewell.samsunggolfmate.utils.MyVolley;
import com.teewell.samsunggolfmate.utils.SharedPreferencesUtils;

public class GuideModel {
	private static GuideModel guideModel;
	private final static String JSON_LABEL_ID = "id";
	private final static String JSON_LABEL_URL = "url";
	private final static String JSON_LABEL_NAME = "name";
	private final static String FILE_PATH = "guide/";
	private final static String regularExpression = "/";
	private SharedPreferencesUtils sharedPreferencesUtils;
	private ArrayList<GuideBean> guideBeanList;
	private Context context;
	private Cache cache;
	public GuideModel(Context context) {
		sharedPreferencesUtils = new SharedPreferencesUtils(context,SharedPreferenceConstant.SHAREDPREFERENCE_NAME);
		this.context = context;
		MyVolley.init(context);
		cache = MyVolley.getRequestQueue().getCache();
	}
	public static GuideModel getInstance(Context context){  
		if (guideModel == null) {
			guideModel = new GuideModel(context);
		}
		return guideModel;
	}
	
	public Cache getCache() {
		return cache;
	}
	public void setCache(Cache cache) {
		this.cache = cache;
	}
	public ArrayList<GuideBean> getGuideBeanList() {
		String json = sharedPreferencesUtils.getString(SharedPreferenceConstant.GUIDE_JSON, null);
		if (json == null) {
//			json = context.getResources().getString(R.string.guide_data);
//			json = SharedPreferenceConstant.GUIDE_DATA;
			InputStream is;
			try {
				is = context.getAssets().open("guideJson.txt");
				BufferedReader br=new BufferedReader(new InputStreamReader(is,"UTF-8")); 
				StringBuffer baos = new StringBuffer();
				String str = null;
				while ((str = br.readLine())!= null) {
					baos.append(str);
				}
				json = baos.toString();
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
				return null;
			}
//			copyToSd();
			sharedPreferencesUtils.commitString(SharedPreferenceConstant.GUIDE_JSON, json);
		}
		guideBeanList = JsonProcessUtil.arrayListFromJSON(json, GuideBean.class);
		return guideBeanList;
	}

	private void copyToSd(){
		 try {
			String files[] = context.getAssets().list(FILE_PATH);
			for (String string : files) {
				String outFileName = Contexts.GUIDE_PATH + string;
				OutputStream myOutput = new FileOutputStream(outFileName);
				InputStream  myInput = context.getAssets().open(FILE_PATH+string);
				byte[] buffer = new byte[myInput.available()];
				int count = 0;
				while ((count = myInput.read(buffer)) > 0)
				{
					myOutput.write(buffer, 0, count);
				}
				myOutput.flush();
				myInput.close();
				myOutput.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	private HttpPost getGuideDataHttpPost(){
		return null;
	}
	public void asyncGuideData(){
		AsyncHttpClient.sendRequest(handler, getGuideDataHttpPost(), new AbstractAsyncResponseListener() {
			@Override
			protected void onSuccess(String response) {
				ArrayList<GuideBean> guideBeanList = JsonProcessUtil.arrayListFromJSON(response, GuideBean.class);
				for (GuideBean guideBean : guideBeanList) {
					boolean isExist = false;
					for (GuideBean bean : GuideModel.this.guideBeanList) {
						if (bean.getUrl().equals(guideBean.getUrl())) {
							isExist = true;
							break;
						}
					}
					if (!isExist) {
						downloadGuideBitmap(guideBean);
					}
				}
			}

			@Override
			protected void onFailure(Throwable e) {
				super.onFailure(e);
			}
			
		});
		
	}
	public void asyncGuide(){
		MyVolley.Get(null, new Listener<String>() {
			@Override
			public void onResponse(String response) {
				String json = null;
				JSONArray jsonArray;
				try {
					jsonArray = new JSONArray(response);
					ArrayList<GuideBean> list = new ArrayList<GuideBean>();
					for (int i = 0; i < jsonArray.length(); i++) {
						GuideBean bean = new GuideBean();
						JSONObject jsonObject = jsonArray.optJSONObject(i);
						bean.setId(jsonObject.optInt(JSON_LABEL_ID));
						String url = jsonObject.optString(JSON_LABEL_URL);
						bean.setUrl("http://192.168.2.126:3000"+url);
						String names[] = url.split(regularExpression);
						bean.setName(names[names.length-1]);
						list.add(bean);
					}
					json = JsonProcessUtil.arrayListToJSON(list);
				} catch (JSONException e) {
					e.printStackTrace();
				}
				sharedPreferencesUtils.commitString(SharedPreferenceConstant.GUIDE_JSON, json);
			}
		}, null);
	}
	private HttpPost getDownloadBitmapHttpPost(){
		return null;
	}
	public void downloadGuideBitmap(GuideBean bean){
		AsyncHttpClient.sendRequest(handler, getDownloadBitmapHttpPost(), new GuideBitmapResponseListener(bean));
	}
	public class GuideBitmapResponseListener implements AsyncResponseListener{
		private GuideBean bean;
		public GuideBitmapResponseListener(GuideBean bean){
			this.bean = bean; 
		}
		
		@Override
		public void onResponseReceived(HttpEntity response) {
			// 获得一个输入流
			InputStream is;
			try {
				is = response.getContent();
				Bitmap bitmap = BitmapFactory.decodeStream(is);
				if (bitmap!=null) {
					String photoPathUrl = bean.getId()+SharedPreferenceConstant.STRING_SPLIT+bean.getName();
					FileOutputStream bos = new FileOutputStream(photoPathUrl);
					bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos);
					bitmap.isRecycled();
					bos.flush();
					bos.close();
				}
				is.close();
				
			} catch (IllegalStateException e) {
			} catch (IOException e) {
			}
		}
		@Override
		public void onResponseReceived(Throwable response) {
			
		}


	}
	public void downloadBitmap(final String url){
		ImageRequest imageRequest = new ImageRequest(url, new Listener<Bitmap>() {
			@Override
			public void onResponse(Bitmap arg0) {
				if (arg0!=null) {
					Log.e("---------", "---------------");
				}
			}
		}, 0, 0, Config.ARGB_8888, null);
		MyVolley.getRequestQueue().add(imageRequest);
	}
	private Handler handler = new Handler(){
		public void handleMessage(android.os.Message msg) {
			switch (msg.what) {
			case 0:
				
				break;

			default:
				break;
			}
		}
	};
}
