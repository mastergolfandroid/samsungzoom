package com.teewell.samsunggolfmate.models;

import java.text.CollationKey;
import java.text.Collator;
import java.text.RuleBasedCollator;
import java.util.Comparator;

import com.teewell.samsunggolfmate.beans.CoursesObject;


public class ComparatorProvinceCity implements Comparator<CoursesObject>{
	
	RuleBasedCollator collator;
	public ComparatorProvinceCity(){
		collator = (RuleBasedCollator)Collator.getInstance(java.util.Locale.CHINA);
	}
	@Override
	public int compare(CoursesObject object1, CoursesObject object2) {
		
		CollationKey p1 = collator.getCollationKey(object1.getProvinceName());
		CollationKey p2 = collator.getCollationKey(object2.getProvinceName());
		if(p1.compareTo(p2) > 0){
			return 1;
		}else if (p1.compareTo(p2) < 0){
			return -1;
		}else{ 
			CollationKey c1 = collator.getCollationKey(object1.getCity());
			CollationKey c2 = collator.getCollationKey(object2.getCity());
			if(c1.compareTo(c2) > 0){
				return 1;
			}else if (c1.compareTo(c2) < 0){
				return -1;
			}else{ 
				return 0;
			}
		}
	}
 
}
