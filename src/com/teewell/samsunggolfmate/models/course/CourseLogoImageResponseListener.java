package com.teewell.samsunggolfmate.models.course;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.http.HttpEntity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.teewell.samsunggolfmate.utils.AsyncResponseListener;
import com.teewell.samsunggolfmate.views.CourseListCellWrapper;

public class CourseLogoImageResponseListener implements AsyncResponseListener{
	private CourseListCellWrapper cellView;
	private String photoPathUrl;
	public CourseLogoImageResponseListener(String photoPathUrl,CourseListCellWrapper cellView){
		this.cellView = cellView; // default type
		this.photoPathUrl = photoPathUrl;
	}
	
	@Override
	public void onResponseReceived(HttpEntity response) {
		// 获得一个输入流
		InputStream is;
		try {
			is = response.getContent();
			Bitmap bitmap = BitmapFactory.decodeStream(is);
			if (bitmap!=null) {
				FileOutputStream bos = new FileOutputStream(photoPathUrl);
				bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos);
//			bitmap.isRecycled();
				cellView.logo.setImageBitmap(bitmap);
				bos.flush();
				bos.close();
			}
			is.close();
			
		} catch (IllegalStateException e) {
//			e.printStackTrace();
		} catch (IOException e) {
//			e.printStackTrace();
		}
	}
	@Override
	public void onResponseReceived(Throwable response) {
		// TODO Auto-generated method stub
		
	}


}
