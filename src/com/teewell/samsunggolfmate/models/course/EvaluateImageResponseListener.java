package com.teewell.samsunggolfmate.models.course;

import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.ParseException;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import android.view.View;

import com.teewell.samsunggolfmate.beans.CoursesObject;
import com.teewell.samsunggolfmate.beans.EvaluateObject;
import com.teewell.samsunggolfmate.utils.AsyncResponseListener;
import com.teewell.samsunggolfmate.views.CourseListCellWrapper;

public class EvaluateImageResponseListener implements AsyncResponseListener{
	@SuppressWarnings("unused")
	private static final String TAG = EvaluateImageResponseListener.class.getName();
	private static final String RESULT_LABEL_RETURNCODE = "returnCode";
	private static final int SUCCESS_RETURNCODE = 1;
	private static final String RESULT_LABEL_DATA = "data";
	private CourseListCellWrapper cellView;
	private CoursesObject course;
	
	public EvaluateImageResponseListener(CoursesObject course,CourseListCellWrapper cellView){
		this.cellView = cellView; // default type
		this.course = course;
	}
	
	@Override
	public void onResponseReceived(HttpEntity response) {
		// TODO Auto-generated method stub
    	try {
			String responseBody = EntityUtils.toString(response);
			JSONObject jsonObject;
			jsonObject = new JSONObject(responseBody);
			int returnCode = jsonObject.optInt(RESULT_LABEL_RETURNCODE);
			if (returnCode == SUCCESS_RETURNCODE) {
				double evaluate = jsonObject.optDouble(RESULT_LABEL_DATA, 0);
				course.setEvaluate(jsonObject.optDouble(RESULT_LABEL_DATA, 0));
				cellView.courseRating.setVisibility(View.VISIBLE);
				cellView.courseRating.setBackgroundResource(EvaluateObject.RATINGBAR_ID[(int)(evaluate*2)]);
			}
		} catch (JSONException e) {
//			e.printStackTrace();
		} catch (ParseException e) {
//			e.printStackTrace();
		} catch (IOException e) {
//			e.printStackTrace();
		}

	}
	@Override
	public void onResponseReceived(Throwable response) {
		// TODO Auto-generated method stub
		
	}


}
