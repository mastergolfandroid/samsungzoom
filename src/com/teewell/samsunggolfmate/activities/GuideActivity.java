package com.teewell.samsunggolfmate.activities;

import java.util.ArrayList;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.teewell.samsunggolfmate.beans.GuideBean;
import com.teewell.samsunggolfmate.models.GuideModel;
import com.teewell.samsunggolfmate.views.GuideFragmentAdapter;

public class GuideActivity extends FragmentActivity {
	private GuideFragmentAdapter mBasePageAdapter;
	private ViewPager vp_guide;
	private RadioGroup vp_progress;
	private ArrayList<GuideBean> guideBeanList;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_guide);
		initControl();
		initData();
		initViewPager();
	}
	 private void initViewPager() {
	        mBasePageAdapter = new GuideFragmentAdapter(this,guideBeanList);       
	        vp_guide.setOffscreenPageLimit(2);
	        vp_guide.setAdapter(mBasePageAdapter);
	        mBasePageAdapter.addFragment();
	        vp_guide.setCurrentItem(0);
	        vp_guide.setOnPageChangeListener(new OnPageChangeListener() {
				
				@Override
				public void onPageSelected(int arg0) {
					((RadioButton)vp_progress.getChildAt(arg0)).setChecked(true);
				}
				
				@Override
				public void onPageScrolled(int arg0, float arg1, int arg2) {
				}
				
				@Override
				public void onPageScrollStateChanged(int arg0) {
				}
			});
	        for (int i = 0; i < guideBeanList.size(); i++) {
				RadioButton child = new RadioButton(getApplicationContext());
				child.setButtonDrawable(R.drawable.setup_indicator_check);
				child.setFocusable(false);
				child.setFocusableInTouchMode(false);
				child.setClickable(false);
				child.setEnabled(false);
				vp_progress.addView(child );
			}
	        vp_progress.setFocusable(false);
	        vp_progress.setFocusableInTouchMode(false);
	        vp_progress.setClickable(false);
	        ((RadioButton)vp_progress.getChildAt(0)).setChecked(true);
	    }
	 private void initControl() {

	        vp_guide = (ViewPager) findViewById(R.id.guide_pager);
	        vp_progress = (RadioGroup)findViewById(R.id.progress_group);
	        vp_progress.setPressed(false);
	        
	 }
	 private void initData(){
		guideBeanList = GuideModel.getInstance(this).getGuideBeanList();
		GuideModel.getInstance(this).asyncGuide();
	 }
	 @Override
	public void onBackPressed() {
//		Intent intent = new Intent(getApplicationContext(), TabHostActivity.class);
//		startActivity(intent);
//		finish();
	}
}
