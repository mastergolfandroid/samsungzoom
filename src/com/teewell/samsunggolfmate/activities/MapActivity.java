package com.teewell.samsunggolfmate.activities;

import java.util.List;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.AbsoluteLayout;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SlidingDrawer;
import android.widget.SlidingDrawer.OnDrawerCloseListener;
import android.widget.SlidingDrawer.OnDrawerOpenListener;
import android.widget.TextView;

import com.polites.android.GestureImageView;
import com.teewell.samsunggolfmate.beans.AerialPhotoBean;
import com.teewell.samsunggolfmate.beans.FairwayObject;
import com.teewell.samsunggolfmate.beans.FuncButtonState;
import com.teewell.samsunggolfmate.beans.MapMarkeVo;
import com.teewell.samsunggolfmate.beans.PlayGameObject;
import com.teewell.samsunggolfmate.beans.ValidAerialPhotoBean;
import com.teewell.samsunggolfmate.beans.WeatherInfoBean;
import com.teewell.samsunggolfmate.common.Contexts;
import com.teewell.samsunggolfmate.common.RountContexts;
import com.teewell.samsunggolfmate.costants.KeyValuesConstant;
import com.teewell.samsunggolfmate.datebase.DataBaseSingle;
import com.teewell.samsunggolfmate.datebase.MyDataBaseAdapter;
import com.teewell.samsunggolfmate.models.MapModel;
import com.teewell.samsunggolfmate.models.weather.WeatherInfoModel;
import com.teewell.samsunggolfmate.utils.BitmapUtil;
import com.teewell.samsunggolfmate.utils.CoordinateUtil;
import com.teewell.samsunggolfmate.utils.DistanceUtil;
import com.teewell.samsunggolfmate.utils.GpsUtil;
import com.teewell.samsunggolfmate.utils.LocationUtil;
import com.teewell.samsunggolfmate.utils.ScreenUtil;
import com.teewell.samsunggolfmate.views.AssistView;

/**
 * 航拍图界面
 * 关键点：计算距离其实主要是两点的经纬度计算
 * 1,人的位置有两个地方决定：a,为获取到Gps时手指拖动，此时可以保存XY及经纬度；b,有Gps时刷新此时可以保存XY及经纬度
 * 2，
 * 
 * @author chengmingyan
 */
public class MapActivity extends ParentActivity implements OnClickListener,
		SensorEventListener {
	private static final String TAG = MapActivity.class.getName();
	//布局相关
	private GestureImageView aerialPhotoView;
	private FrameLayout layout_map;
	private AssistView assistView;
	private ProgressBar progressBar;
	private ImageView image_wind;
	private ImageView personView;
	private TextView tv_wind;
	private SlidingDrawer drawer;
	private LinearLayout layout_buttonGroup;
	private LinearLayout btn_zoom, btn_cursor, btn_addMark, btn_removeMarke;
	private FrameLayout layout_wind;
	private AbsoluteLayout layout_person;
	private Bitmap flagBmp, flagBmp1, flagBmp2;
	private AnimationDrawable animationDrawable;
	// private AnimationDrawable loadingAinm;
//	数据相关
	private MapModel model;
	private GpsUtil gpsUtil;
	private CoordinateUtil util;
	private AerialPhotoBean aerialPhotoBean;
	private List<FairwayObject> holesList;
	private List<AerialPhotoBean> aerialPhotoBeans;
	private int wind_angle;
	private int lastHoleIndex;// 上一次球洞号
	private MyDataBaseAdapter db;
//	private PlayGameObject playGameObject;
	public boolean state_direction;
//	常量相关
	
	public static final float INITIAL_ROTATION = -90;
	private static final int WHAT_CHANGE_MAP = 0x0;
	private static final int WHAT_CHANGE_WIND = 0x1;
	public static final int WHAT_CHANGE_MENUBAR = 0x2;
	public static final int WHAT_CHANGE_DIRECTION = 0x3;
	public static final int WHAT_CHANGE_COURSEBITMAP = 0x4;
	public static final int WHAT_DISTANCETRACKING = 0x5;
	public static final int WHAT_LOCATION_REFRESH = 0x10;
	private static final String DIRECTION[] = { "北风", "南风", "西风", "东风", "西北风",
			"东北风", "西南风", "东南风" };
	private static final int ANGLE[] = { -90, -270, 0, -180, -45, -135, -315,
			-225 };
	public static final String ACTION_BROADCASTRECEIVER = "com.teewell.golfmate.activities.MapActivity.MapBroadcastReceiver";
	
	

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.map_layout);
//		自初始化一次的
//		playGameObject = (PlayGameObject) getIntent().getSerializableExtra(
//				KeyValuesConstant.INTENT_PLAYGAMEOBJECT);
		model = MapModel.getInstance(this);
		gpsUtil = GpsUtil.getInstance(this);
		lastHoleIndex = -1;
		
		initView();

		WeatherInfoBean weatherInfoBean = WeatherInfoModel.getInstance(
				this.getApplicationContext()).getWeatherInfoBean(
				RountContexts.playGameObject.getCoursesObject().getCity());

		if (weatherInfoBean != null) {
			tv_wind.setText(weatherInfoBean.getWS());
			for (int i = 0; i < DIRECTION.length; i++) {
				if (DIRECTION[i].equals(weatherInfoBean.getWD())) {
					wind_angle = ANGLE[i];
					break;
				}
			}
		}
		initData();
	}

	private void initData() {
		holesList = RountContexts.getFairwayObjectArray(RountContexts.playGameObject);
		aerialPhotoBeans = RountContexts.playGameObject.getAerialPhotoBeanArray();
	}

	private void initView() {
		layout_map = (FrameLayout) findViewById(R.id.layout_map);
		progressBar = (ProgressBar) findViewById(R.id.progressBar);
		aerialPhotoView = (GestureImageView) findViewById(R.id.photoView);
		assistView = (AssistView) findViewById(R.id.assistView);

		layout_person = (AbsoluteLayout) findViewById(R.id.layout_person);
		personView = (ImageView) findViewById(R.id.personView);
		personView.setBackgroundResource(R.drawable.person);
		// personView.setBackgroundResource(R.anim.person);
		// loadingAinm = (AnimationDrawable) personView.getBackground();
		// personView.post(new Runnable() {
		// public void run() {
		// loadingAinm.start();
		// }
		// });

		Button btn_evaluate = (Button) findViewById(R.id.btn_evaluate);
		CheckBox btn_location = (CheckBox) findViewById(R.id.btn_location);
		Button btn_share = (Button) findViewById(R.id.btn_share);
		CheckBox btn_wind = (CheckBox) findViewById(R.id.btn_wind);
		Button btn_more = (Button) findViewById(R.id.btn_more);
		drawer = (SlidingDrawer) findViewById(R.id.slidingDrawer1);
		drawer.setOnDrawerCloseListener(new OnDrawerCloseListener() {
			@Override
			public void onDrawerClosed() {
				drawer.getHandle().setBackgroundResource(
						R.drawable.slidingsrawer_btn_open);
			}
		});
		drawer.setOnDrawerOpenListener(new OnDrawerOpenListener() {
			@Override
			public void onDrawerOpened() {
				drawer.getHandle().setBackgroundResource(
						R.drawable.slidingsrawer_btn_close);
			}
		});
		// cursorAndMarkView.setOnTouchListener(new OnTouchListener() {
		//
		// @Override
		// public boolean onTouch(View v, MotionEvent event) {
		// // TODO Auto-generated method stub
		// if (drawer.isOpened()) {
		// drawer.close();
		// }
		// cursorAndMarkView.onTouchEvent(event);
		// return false;
		// }
		// });
		btn_evaluate.setOnClickListener(this);
		btn_location.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				changePersonState(isChecked);

			}
		});
		btn_share.setOnClickListener(this);
		btn_wind.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				if (isChecked) {
					handler.sendEmptyMessage(WHAT_CHANGE_WIND);
				} else {
					animationDrawable.stop();
					image_wind.clearAnimation();
					/** 通知imageView 刷新屏幕 **/
					image_wind.postInvalidate();
					layout_wind.setVisibility(View.INVISIBLE);
				}
			}
		});
		btn_more.setOnClickListener(this);

		image_wind = (ImageView) findViewById(R.id.image_wind);
		tv_wind = (TextView) findViewById(R.id.tv_wind);
		/** 通过ImageView对象拿到背景显示的AnimationDrawable **/
		animationDrawable = (AnimationDrawable) image_wind.getDrawable();
		layout_wind = (FrameLayout) findViewById(R.id.layout_wind);
		layout_wind.setVisibility(View.INVISIBLE);

		personBmp_location = BitmapFactory.decodeResource(getResources(),
				R.drawable.personwithdirection);

		layout_buttonGroup = (LinearLayout) findViewById(R.id.layout_buttonGroup);
		btn_zoom = (LinearLayout) findViewById(R.id.btn_zoom);
		btn_cursor = (LinearLayout) findViewById(R.id.btn_cursor);
		btn_addMark = (LinearLayout) findViewById(R.id.btn_addMark);
		btn_removeMarke = (LinearLayout) findViewById(R.id.btn_removeMark);
		
		btn_zoom.setOnClickListener(this);
		btn_cursor.setOnClickListener(this);
		btn_addMark.setOnClickListener(this);
		btn_removeMarke.setOnClickListener(this);
		btn_removeMarke.setOnLongClickListener(new OnLongClickListener() {
			@Override
			public boolean onLongClick(View v) {
				AlertDialog.Builder dialog = new AlertDialog.Builder(MapActivity.this);
				dialog.setMessage("删除所有标记点?");
				dialog.setNegativeButton("取消", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				});
				dialog.setPositiveButton("确定", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						db.deleteMapMarkeByHoleID(aerialPhotoBean.getAerialPhotoID());
						model.getMapMarkeVoList().clear();;
						dialog.dismiss();
						assistView.invalidate();
					}
				});
				dialog.show();
				return false;
			}
		});
		model.setFuncButtonState(FuncButtonState.cursor);
		model.setButtonGroupView(layout_buttonGroup);
		
		// 指示果岭的图片
		flagBmp = BitmapFactory.decodeResource(getResources(), R.drawable.flag);
		flagBmp1 = BitmapFactory.decodeResource(getResources(),
				R.drawable.flag1);
		flagBmp2 = BitmapFactory.decodeResource(getResources(),
				R.drawable.flag2);
		
		flagBmp = BitmapUtil.zoomBitmap(flagBmp, 0.5f);
		flagBmp1 = BitmapUtil.zoomBitmap(flagBmp1, 0.25f);
		flagBmp2 = BitmapUtil.zoomBitmap(flagBmp2, 0.25f);
	}

	private void changeAerialPhotoThread() {
		aerialPhotoBean = aerialPhotoBeans.get(RountContexts.curHoleNumber);
		progressBar.setVisibility(View.VISIBLE);
		new Thread() {
			@Override
			public void run() {
				Bitmap bitmapOrg = BitmapUtil
						.readBitMapByNative(Contexts.AERIALPHOTO_PATH
								+ RountContexts.playGameObject.getCoursesObject()
										.getCouseID() + "/"
								+ aerialPhotoBean.getPhotoName() + "."
								+ aerialPhotoBean.getPhotoMode());
				Log.d(TAG, "bitmap.width" + bitmapOrg.getWidth()
						+ "-bitmap.height" + bitmapOrg.getHeight());
				int validArea[] = BitmapUtil.getValidArea(bitmapOrg);
				Message message = new Message();
				message.what = WHAT_CHANGE_MAP;
				ValidAerialPhotoBean bean = new ValidAerialPhotoBean();
				bean.setBitmap(bitmapOrg);
				bean.setValidArea(validArea);
				message.obj = bean;
				handler.sendMessage(message);
				super.run();
			}
		}.start();
	}

	@SuppressLint("HandlerLeak")
	Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			switch (msg.what) {
			case WHAT_CHANGE_MAP:
				progressBar.setVisibility(View.GONE);
				initMap((ValidAerialPhotoBean) msg.obj);
				break;
			case WHAT_CHANGE_WIND:
				// 功能：构建一个旋转动画。
				// 参数：fromDegress 为开始的角度； toDegress 为结束的角度。
				// pivotXType、pivotYType分别为x、y 的伸缩模式。
				// pivotXValue、pivotYValue 分别为伸缩动画相对于x、y的坐标的开始位置。
				image_wind.clearAnimation();
				RotateAnimation mLeftAnimation = new RotateAnimation(0.0f,
						wind_angle + util.getRotateAngle(),
						Animation.RELATIVE_TO_SELF, 0.5f,
						Animation.RELATIVE_TO_SELF, 0.5f);
				mLeftAnimation.setDuration(200);
				mLeftAnimation.setFillAfter(true);
				mLeftAnimation.setFillEnabled(true);
				layout_wind.setVisibility(View.VISIBLE);
				image_wind.startAnimation(mLeftAnimation);
				animationDrawable.start();// 开始这个动画
				/** 通知imageView 刷新屏幕 **/
				image_wind.postInvalidate();
				break;

			case WHAT_CHANGE_DIRECTION:
				isChangeDirection = false;
				break;
			case WHAT_LOCATION_REFRESH:
				assistView.invalidate();
				model.refreshPersonPoint(MapActivity.this);
				refreshPersonView();
				break;
			case WHAT_DISTANCETRACKING:
				if (danceDiaLog) {
					distance = DistanceUtil.getDistance(gpsUtil.latitude,
							gpsUtil.longitude, curLat, curLon,
							RountContexts.shortDistanceIndex)
							+ "";
					distanceTextView.setText(distance + distanceUnit_near);
				}
				break;
			case WHAT_CHANGE_COURSEBITMAP:
				float ratio = aerialPhotoView.getScale();
				float bmpHeight = model.getCourseBitmap().getHeight()*ratio;
				float bmpWidth = model.getCourseBitmap().getWidth()*ratio;
				Log.e(TAG, "bitmapHeight=" + bmpHeight + ",bitmapWidth="
						+ bmpWidth);
				util.resetAttribute(ratio, aerialPhotoView.getImageX()
						- bmpWidth / 2, aerialPhotoView.getImageY() - bmpHeight
						/ 2, bmpWidth, bmpHeight);
				model.getGreenPointArray().clear();
				if (aerialPhotoBean.getHoleId()[1] > 0) {
					for (FairwayObject fairwayObject : holesList) {
						if (aerialPhotoBean.getAerialPhotoID().intValue() == fairwayObject
								.getAerialPhotoBean().getAerialPhotoID().intValue()) { 
							model.addGreenPoint(
									Double.parseDouble(fairwayObject
											.getGreenLongitude()),
									Double.parseDouble(fairwayObject.getGreenLatitude()));
						}
					}
				} else {
					for (FairwayObject fairwayObject : holesList) {
						if (aerialPhotoBean.getAerialPhotoID().intValue() == fairwayObject
								.getAerialPhotoBean().getAerialPhotoID().intValue()) { 
							model.addGreenPoint(
									Double.parseDouble(fairwayObject
											.getGreenLongitude()),
									Double.parseDouble(fairwayObject.getGreenLatitude()));
							break;
						}
					}
				}
				model.refreshData(MapActivity.this);
				refreshMarke();
				
				aerialPhotoView.invalidate();
				assistView.setNavigationMapModel(model, handler);
				assistView.invalidate();
				refreshPersonView();
				break;
			default:
				break;
			}
		};
	};
	

	private void initMap(ValidAerialPhotoBean bean) {
		Bitmap bitmapOrg = bean.getBitmap();
		int validArea[] = bean.getValidArea();
		try {
			if (bitmapOrg == null || aerialPhotoBean == null) {
				return;
			}
			if (model.getFuncButtonState()==FuncButtonState.addMark||model.getFuncButtonState()==FuncButtonState.removeMark) {
				if (lastHoleIndex > -1) {
					model.changeMarkeVo(db, aerialPhotoBeans.get(lastHoleIndex)
							.getAerialPhotoID(),-1);
				} else {
					model.changeMarkeVo(db, -1,-1);
				}
			}else if (lastHoleIndex!=RountContexts.curHoleNumber&&model.getMapMarkeVoList()!=null) {
				model.saveMapMarkVoList(db, aerialPhotoBeans.get(lastHoleIndex)
						.getAerialPhotoID());
			}
			
			util = new CoordinateUtil(validArea,
					aerialPhotoBean.getRotationView(),
					Double.parseDouble(aerialPhotoBean.getLuLongitude()),
					Double.parseDouble(aerialPhotoBean.getLuLatitude()),
					Double.parseDouble(aerialPhotoBean.getRdLongitude()),
					Double.parseDouble(aerialPhotoBean.getRdLatitude()));
			model.setCoordinateUtil(util);
//			以下是将果岭旗帜画在航拍图上，生成新的航拍图保存，并使用新的航拍图作为背景
			Bitmap bitmap = Bitmap.createBitmap(bitmapOrg.getWidth(),
					bitmapOrg.getHeight(), Config.RGB_565);
			Canvas canvas = new Canvas(bitmap);
			Paint defaultPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
			defaultPaint.setStyle(Paint.Style.STROKE);
			canvas.drawBitmap(bitmapOrg, 0, 0, defaultPaint);
			float bmpHeight = bitmapOrg.getHeight();
			float bmpWidth = bitmapOrg.getWidth();
			double onePxToLon = Math.abs((util.getLonRBottom() - util
					.getLonLTop()) / bmpWidth);
			double onePxToLat = Math.abs((util.getLatRBottom() - util
					.getLatLTop()) / bmpHeight);
			double midpointX = bmpWidth / 2;
			double midpointY = -bmpHeight / 2;
			float rotateAngle = util.getRotateAngle();
			int greenCount = 0;
			boolean isDoubleGreen = false;
			if (aerialPhotoBean.getHoleId()[1] > 0) {
				isDoubleGreen = true;
			}
//			model.getGreenPointArray().clear();
			for (FairwayObject fairwayObject : holesList) {
				if (aerialPhotoBean.getAerialPhotoID().intValue() == fairwayObject
						.getAerialPhotoBean().getAerialPhotoID().intValue()) {
					double greenLon = Double.parseDouble(fairwayObject
							.getGreenLongitude());
					double greenLat = Double.parseDouble(fairwayObject
							.getGreenLatitude());

//					model.addGreenPoint(greenLon, greenLat);
					double x1 = (greenLon - util.getLonLTop()) / onePxToLon;
					double y1 = (util.getLatLTop() - greenLat) / onePxToLat;
					y1 = -y1;
					float bitmapX = (float) (midpointX + (x1 - midpointX)
							* Math.cos(-rotateAngle * 2 * Math.PI / 360) - (y1 - midpointY)
							* Math.sin(-rotateAngle * 2 * Math.PI / 360));
					float bitmapY = (float) (midpointY + (x1 - midpointX)
							* Math.sin(-rotateAngle * 2 * Math.PI / 360) + (y1 - midpointY)
							* Math.cos(-rotateAngle * 2 * Math.PI / 360));
					bitmapY = -bitmapY;
					if (isDoubleGreen) {
						if (greenCount > 0) {
							canvas.drawBitmap(flagBmp2, bitmapX, bitmapY
									- flagBmp2.getHeight(), defaultPaint);
						} else {
							canvas.drawBitmap(flagBmp1, bitmapX, bitmapY
									- flagBmp1.getHeight(), defaultPaint);
							greenCount++;
						}
					} else {
						canvas.drawBitmap(flagBmp, bitmapX,
								bitmapY - flagBmp.getHeight(), defaultPaint);
						break;
					}

				}
			}
			if (!bitmapOrg.isRecycled()) {
				bitmapOrg.recycle();
			}
			if (model.getCourseBitmap()!=null&&!model.getCourseBitmap().isRecycled()) {
				model.getCourseBitmap().recycle();
			}
			model.setCourseBitmap(bitmap);

			aerialPhotoView.setRecycle(true);
			aerialPhotoView.setImageBitmap(bitmap, model, assistView, handler);
			if (layout_wind.getVisibility() == View.VISIBLE) {
				handler.sendEmptyMessage(WHAT_CHANGE_WIND);
			}
			
		} catch (Exception e) {
			Log.e(TAG, "", e);
		}
	}

	private void refreshMarke(){
		if (model.getFuncButtonState()==FuncButtonState.addMark||model.getFuncButtonState()==FuncButtonState.removeMark) {
			if (lastHoleIndex > -1) {
				model.changeMarkeVo(db,-1, aerialPhotoBeans.get(RountContexts.curHoleNumber)
						.getAerialPhotoID());
			}
		}else if (model.getMapMarkeVoList()!=null && model.getFuncButtonState() == FuncButtonState.zoom) {
				model.setMapMarkeVoList(model.getMapMarkeVoList());
		}
		lastHoleIndex = RountContexts.curHoleNumber;
	}

	@Override
	protected void onPause() {
		super.onPause();
		unregisterReceiver(MapBroadcastReceiver);
		if (model.getMapMarkeVoList()!=null) {
			model.saveMapMarkVoList(db, aerialPhotoBeans.get(RountContexts.curHoleNumber)
					.getAerialPhotoID());
		}
		if (state_direction) {
			SensorManager sensor_manager = (SensorManager) getSystemService(SENSOR_SERVICE); // 获取管理服务
			sensor_manager.unregisterListener(this);
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		registerBoradcastReceiver();

		gpsUtil.setHandlerInfo(handler, WHAT_LOCATION_REFRESH,
				LocationUtil.LOC_PERIOD);
		db = DataBaseSingle.getInstance(this).getDataBase();
		changeMapByHoleID();
		changePersonState(state_direction);
	}

	private void changePersonState(boolean isChecked) {

		SensorManager sensor_manager = (SensorManager) getSystemService(SENSOR_SERVICE); // 获取管理服务
		if (isChecked) {
			// if (loadingAinm.isRunning()) {
			// loadingAinm.stop();
			// }
			personView.setBackgroundResource(R.drawable.personwithdirection);
			// 注册监听器
			sensor_manager.registerListener(MapActivity.this,
					sensor_manager.getDefaultSensor(Sensor.TYPE_ORIENTATION),
					SensorManager.SENSOR_DELAY_GAME);
		} else {
			degree = 0;
			if (state_direction) {
				sensor_manager.unregisterListener(MapActivity.this);
			}
			personView.setBackgroundResource(R.drawable.person);
			// personView.setBackgroundResource(R.anim.person);
			// loadingAinm = (AnimationDrawable) personView.getBackground();
			// personView.post(new Runnable() {
			// public void run() {
			// loadingAinm.start();
			// }
			// });
		}
		state_direction = isChecked;
	}

	private void changeMapByHoleID() {
		// 同一洞且子球场不改变
		if (lastHoleIndex == RountContexts.curHoleNumber
				&& !RountContexts.isChangeSubCourse) {
			return;
		}
		if (RountContexts.isChangeSubCourse) {
			initData();
			RountContexts.isChangeSubCourse = false;
		}
		
		changeAerialPhotoThread();

	}

	BroadcastReceiver MapBroadcastReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			if (action.equals(ACTION_BROADCASTRECEIVER)) {
				// Toast.makeText(ObstadeActivity.this,
				// "处理action名字相对应的广播"+RountContexts.curHoleNumber, 200);
				Log.e(TAG, "map广播接收");
				changeMapByHoleID();
			}
		}
	};
	private int requestCode_courseSetting = 0x1;
	private AlertDialog distanceDialog;
	private boolean danceDiaLog;
	private TextView distanceTextView;
	private boolean hideFlag;
	private String distance = "0";
	private String distanceUnit_near;
	private double curLat;
	private double curLon;

	private void registerBoradcastReceiver() {
		IntentFilter myIntentFilter = new IntentFilter();
		myIntentFilter.addAction(ACTION_BROADCASTRECEIVER);
		// 注册广播
		registerReceiver(MapBroadcastReceiver, myIntentFilter);
	}

	private void getDistanceDialog() {
		danceDiaLog = true;
		distanceDialog = new AlertDialog.Builder(this).create();
		distanceDialog.show();
		distanceDialog.getWindow().setContentView(R.layout.dialog_distance);
		WindowManager.LayoutParams layoutParams = distanceDialog.getWindow()
				.getAttributes();
		layoutParams.width = ScreenUtil.getScreenWidth(this);
		Log.e(TAG, "width=" + layoutParams.width);
		distanceDialog.getWindow().setAttributes(layoutParams);
		distanceDialog.getWindow().setGravity(Gravity.BOTTOM);
		// distanceDialog.setCancelable(false);
		distanceDialog.setOnCancelListener(new OnCancelListener() {
			private boolean hideFlag;

			@Override
			public void onCancel(DialogInterface dialog) {
				// TODO Auto-generated method stub
				hideFlag = true;
				danceDiaLog = false;
				gpsUtil.resetHandlerInfo(handler, WHAT_LOCATION_REFRESH);
			}
		});
		LayoutParams btnLayoutParams = null;
		LinearLayout layout_distance = (LinearLayout) distanceDialog
				.findViewById(R.id.layout_distance);
		distanceTextView = (TextView) distanceDialog
				.findViewById(R.id.distance_text);
		btnLayoutParams = layout_distance.getLayoutParams();
		btnLayoutParams.width = ScreenUtil.getScreenWidth(this) - 100;
		layout_distance.setLayoutParams(btnLayoutParams);
		Button reset = (Button) distanceDialog
				.findViewById(R.id.distance_reset);
		btnLayoutParams = reset.getLayoutParams();
		btnLayoutParams.width = ScreenUtil.getScreenWidth(this) - 100;
		reset.setLayoutParams(btnLayoutParams);
		reset.setOnClickListener(this);

		Button hide = (Button) distanceDialog.findViewById(R.id.distance_hide);
		btnLayoutParams = hide.getLayoutParams();
		btnLayoutParams.width = ScreenUtil.getScreenWidth(this) - 100;
		hide.setLayoutParams(btnLayoutParams);
		hide.setOnClickListener(this);

		if (!hideFlag) {
			curLat = gpsUtil.latitude;
			curLon = gpsUtil.longitude;
		}
		distanceUnit_near = DistanceUtil.getDistanceUnit(this,
				RountContexts.shortDistanceIndex);
		distanceTextView.setText(distance + distanceUnit_near);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.distance_reset: {
			hideFlag = false;
			distance = "0";
			distanceTextView.setText(distance + distanceUnit_near);
			curLat = gpsUtil.latitude;
			curLon = gpsUtil.longitude;
		}
			break;
		case R.id.distance_hide:
			hideFlag = true;
			danceDiaLog = false;
			distanceDialog.dismiss();
			gpsUtil.resetHandlerInfo(handler, WHAT_LOCATION_REFRESH);
			break;
		case R.id.btn_share: {
			gpsUtil.resetHandlerInfo(handler,WHAT_DISTANCETRACKING);
			distanceDialog = null;
			getDistanceDialog();
			distanceDialog.show();
		}
			break;
		case R.id.btn_evaluate: {
			Intent intent = new Intent(MapActivity.this,
					EvaluateOursActivity.class);
			intent.putExtra(KeyValuesConstant.INTENT_COURSESOBJECT,
					RountContexts.playGameObject.getCoursesObject());
			startActivity(intent);
		}
			break;
		case R.id.btn_more: {
			Intent intent = new Intent(MapActivity.this,
					CourseSettingActivity.class);
			startActivityForResult(intent, requestCode_courseSetting);
		}
			break;
		case R.id.btn_zoom:
			btn_zoom.setBackgroundResource(R.drawable.bg_cursor);
			btn_cursor.setBackgroundResource(R.color.black);
			btn_addMark.setBackgroundResource(R.color.black);
			btn_removeMarke.setBackgroundResource(R.color.black);
			model.setFuncButtonState(FuncButtonState.zoom);
			assistView.invalidate();
			
			break;
		case R.id.btn_cursor:
			btn_zoom.setBackgroundResource(R.color.black);
			btn_cursor.setBackgroundResource(R.drawable.bg_cursor);
			btn_addMark.setBackgroundResource(R.color.black);
			btn_removeMarke.setBackgroundResource(R.color.black);
			model.setFuncButtonState(FuncButtonState.cursor);
			assistView.invalidate();
			break;
		case R.id.btn_addMark:
			btn_zoom.setBackgroundResource(R.color.black);
			btn_cursor.setBackgroundResource(R.color.black);
			btn_addMark.setBackgroundResource(R.drawable.bg_cursor);
			btn_removeMarke.setBackgroundResource(R.color.black);
			model.setFuncButtonState(FuncButtonState.addMark);
			if (model.getMapMarkeVoList()==null) {
				List<MapMarkeVo> list = db.selectMarkeByHoleId( aerialPhotoBeans.get(RountContexts.curHoleNumber)
						.getAerialPhotoID());
				model.setMapMarkeVoList(list);
			}
			assistView.invalidate();
			break;
		case R.id.btn_removeMark:
			btn_zoom.setBackgroundResource(R.color.black);
			btn_cursor.setBackgroundResource(R.color.black);
			btn_addMark.setBackgroundResource(R.color.black);
			btn_removeMarke.setBackgroundResource(R.drawable.bg_cursor);
			model.setFuncButtonState(FuncButtonState.removeMark);
			if (model.getMapMarkeVoList()==null) {
				List<MapMarkeVo> list = db.selectMarkeByHoleId( aerialPhotoBeans.get(RountContexts.curHoleNumber)
						.getAerialPhotoID());
				model.setMapMarkeVoList(list);
			}
			assistView.invalidate();
			break;
		default:
			break;
		}
	}

	// 传感器值改变
	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
	}

	boolean isChangeDirection;
	Bitmap personBmp_location;
	private float degree;

	// 精度改变
	@Override
	public void onSensorChanged(SensorEvent event) {
		// 获取触发event的传感器类型
		int sensorType = event.sensor.getType();
		switch (sensorType) {
		case Sensor.TYPE_ORIENTATION:
			if (!isChangeDirection) {
				isChangeDirection = true;
				float degree = event.values[2]; // 获取z转过的角度(当前即垒集）
				RotateAnimation rotateAnimation = new RotateAnimation(
						this.degree, degree, Animation.RELATIVE_TO_SELF, 0.5f,
						Animation.RELATIVE_TO_SELF, 0.5f);
				long time = (long) Math.abs(((degree - this.degree) * 10));
				this.degree = degree;
				rotateAnimation.setDuration(time);
				rotateAnimation.setFillAfter(true);
				personView.startAnimation(rotateAnimation);

				handler.sendEmptyMessageDelayed(WHAT_CHANGE_DIRECTION, time);
			}
			break;
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == requestCode_courseSetting) {
			assistView.initDistance(this);
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	public void refreshPersonView() {
		
		// 画小人
		int x = (int) (model.getPersonPoint().x - (float) personView
				.getMeasuredWidth() / 2);
		int y = (int) (model.getPersonPoint().y - (float) personView
				.getMeasuredHeight() / 2);
		if (x<=0||y<=0) {
			personView.setVisibility(View.GONE);
		}else {
			personView.setVisibility(View.VISIBLE);
			personView.setLayoutParams(new AbsoluteLayout.LayoutParams(
					LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, x, y));
		}

	}


}