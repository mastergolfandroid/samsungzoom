package com.teewell.samsunggolfmate.activities;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Message;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.teewell.darkwarlords.share.AuthorizedOperation;
import com.teewell.darkwarlords.share.utils.DataOperationUtil;
import com.teewell.darkwarlords.share.utils.ShareHandler;
import com.teewell.samsunggolfmate.beans.PlayGameObject;
import com.teewell.samsunggolfmate.beans.ScoreBitmap;
import com.teewell.samsunggolfmate.common.Contexts;
import com.teewell.samsunggolfmate.common.RountContexts;
import com.teewell.samsunggolfmate.costants.KeyValuesConstant;
import com.teewell.samsunggolfmate.models.PhotoModel;

public class ShareCoursePhotoActivity extends ParentActivity implements OnClickListener
{
	private static final String TAG = ShareCoursePhotoActivity.class.getSimpleName();
	private Button btn_cancellation,btn_share;
	private EditText shareText = null; 
	private ImageView deleteImgBtn = null; 
	private ImageView imageID = null; 
	private TextView screenNameT = null; 
	private int inittitle = 0;
	private int share =1;
	
	private TextView contentLen = null; 
	private LinearLayout clearContent = null; 
	private FrameLayout flPic = null;
//	private int screenheight;
//	private int screenwidth;
	private String filePath;
	private PlayGameObject playGameObject;
	public static final String PHOTO_DATA = "photo_data";
	@Override
	protected void onCreate ( Bundle savedInstanceState )
	{
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.share);
		DisplayMetrics dm = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(dm);
		playGameObject = (PlayGameObject) getIntent().getSerializableExtra(
				KeyValuesConstant.INTENT_PLAYGAMEOBJECT);
//		screenheight = dm.widthPixels;
//		screenwidth = dm.heightPixels;
		shareText = (EditText) findViewById(R.id.shareText);
		btn_share = (Button) findViewById(R.id.btn_rightTitle);
		btn_share.setText(getString(R.string.share).toString());
		shareText.setText("今天在@"+playGameObject.getCoursesObject().getCoursesName()+"@"+RountContexts.curHoleNumber+1+"号洞打球,风景优美.@"+getString(R.string.app_name));
		btn_cancellation = (Button) findViewById(R.id.btn_back);
		btn_cancellation.setText(R.string.cancellation);
		deleteImgBtn = (ImageView) findViewById(R.id.deleteImgBtn);
		imageID = (ImageView) findViewById(R.id.imageID);
		screenNameT = (TextView) findViewById(R.id.tv_titleName);
		flPic = (FrameLayout) findViewById(R.id.flPic);
		contentLen = (TextView) findViewById(R.id.contentLen);
		clearContent = (LinearLayout) findViewById(R.id.clearContent);

		btn_share.setOnClickListener(this);
		btn_cancellation.setOnClickListener(this);
		deleteImgBtn.setOnClickListener(this);
		clearContent.setOnClickListener(this);
		imageID.setOnClickListener(this);
		initPhoto(null);

		//先通过isSessionValid方法，把本地文件中保存的授权拿出来
		boolean isSession = AuthorizedOperation.isSessionValid(ShareCoursePhotoActivity.this);
	
		//授权是否可用
		if (isSession == true)
		{
//			String screenName;
//			
//			screenName = com.teewell.darkwarlords.share.Share.getScreenName(ShareCoursePhotoActivity.this);
//			if (!TextUtils.isEmpty(screenName))
//			{
//				screenNameT.setText(screenName);
//			}
//			else
//			{
//				screenNameT.setText("已登录");
//			}
			new Thread(new Runnable() {
				
				@Override
				public void run() {
					// TODO Auto-generated method stub
					String screenName = com.teewell.darkwarlords.share.Share.getScreenName(ShareCoursePhotoActivity.this);
					
					Message msg = new Message();
					msg.obj= GETSCREENNAME;
					Bundle bundle = new Bundle();
					bundle.putString(GETSCREENNAME, screenName);
					msg.setData(bundle);
					msg.what = inittitle;
					shareHandler.sendMessage(msg);
				}
			}).start();
			btn_cancellation.setVisibility(View.VISIBLE);
		} else {
			screenNameT.setText("未登录");
			btn_cancellation.setVisibility(View.GONE);
		}
	}

	private Bitmap initPhoto (String name){
		
		flPic.setVisibility(View.VISIBLE);
		ArrayList<String> list_photoPaths = getIntent().getStringArrayListExtra(PHOTO_DATA);
		Bitmap bitmap = ScoreBitmap.createShareBitmap(this, list_photoPaths);
//		Bitmap bitmap = BitmapUtil.compressImage(rebitmap);
//		imageID.setImageBitmap();
		imageID.setImageBitmap(bitmap);
		filePath = Contexts.SHARE_COURSE_PATH_NAME;
		DataOperationUtil.save2File(bitmap, filePath);
		return bitmap;
	}

	@Override
	public void onClick ( View v ){
		switch (v.getId()){
		
		//授权+分享
			case R.id.btn_rightTitle:
				if (!"".equals(shareText.getText().toString()) || (imageID != null && imageID.getDrawable() != null))
				{
					AuthorizedOperation.requestAccestoken(ShareCoursePhotoActivity.this,
							shareHandler);
				}
				break;
//		 注销
		 case R.id.btn_back:
			 new Thread(new Runnable() {
				
				@Override
				public void run() {
					// TODO Auto-generated method stub
					 AuthorizedOperation.endSession(ShareCoursePhotoActivity.this);
				}
			}).start();
		 screenNameT.setText("未登录");
		 showToast( "已注销" );
		 btn_cancellation.setVisibility(View.GONE);
		 break;
				//删除图片
			case R.id.deleteImgBtn:{
				final Builder dialog = new AlertDialog.Builder(this);
				dialog.setTitle(getString(R.string.apk_title));
				dialog.setMessage(getString(R.string.del_pic));
				dialog.setPositiveButton(getString(R.string.ok),new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						
						flPic.setVisibility(View.GONE);
						filePath = null;
						imageID.setImageBitmap(null);
					}
				});
				dialog.setNegativeButton(getString(R.string.return_),new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				});
				dialog.create().show();
			}break;
			//删除微博内容
			case R.id.clearContent:{
				final Builder dialog = new AlertDialog.Builder(this);
				dialog.setTitle(getString(R.string.apk_title));
				dialog.setMessage(getString(R.string.delete_all));
				dialog.setPositiveButton(getString(R.string.ok),new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						shareText.setText("");
						contentLen.setText("140");
						contentLen.setTextColor(R.color.text_num_gray);
					}
				});
				dialog.setNegativeButton(getString(R.string.return_),new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				});
				dialog.create().show();
			}break;
			//浏览图片
			case R.id.imageID:
				Intent intent = new Intent(ShareCoursePhotoActivity.this,SimplePhotoShowActivity.class);
				intent.putExtra(SimplePhotoShowActivity.EXTRA_PIC_URI, filePath);
				intent.putExtra(SimplePhotoShowActivity.IS_SCORECARD, false);
				startActivity(intent);
		}
	}
	public String GETSCREENNAME = "getscreenname";
	private ShareHandler shareHandler = new ShareHandler()
	{

		@Override
		public void handleMessage ( Message msg )
		{
			//授权成功后执行分享
			if (msg.obj.equals(ShareHandler.AUTH_SUCESS_EN))
			{
				new Thread(new Runnable() {
					
					@Override
					public void run() {
						// TODO Auto-generated method stub
						String screenName = com.teewell.darkwarlords.share.Share.getScreenName(ShareCoursePhotoActivity.this);
						
						Message msg = new Message();
						msg.obj= GETSCREENNAME;
						msg.what = share;
						Bundle bundle = new Bundle();
						bundle.putString(GETSCREENNAME, screenName);
						msg.setData(bundle);
						shareHandler.sendMessage(msg);
					}
				}).start();
				
			}
			if (msg.obj.equals(GETSCREENNAME)) {
				String screenName = msg.getData().getString(GETSCREENNAME);
				if (!TextUtils.isEmpty(screenName))
				{
					screenNameT.setText(screenName);
				}
				else
				{
					screenNameT.setText("加载失败...");
				}
				if(msg.what == ShareCoursePhotoActivity.this.share){
					//判断内容长度是否满足条件
					if (com.teewell.darkwarlords.share.Share.isMeetTheContent(shareText.getText().toString()))
					{
						new Thread(
								new Runnable(){
									@Override
									public void run() {
										if (TextUtils.isEmpty(filePath))
										{
											com.teewell.darkwarlords.share.Share.update(ShareCoursePhotoActivity.this,
													"", shareText.getText().toString(), "", "",
													shareHandler);
										}
										else
										{
											com.teewell.darkwarlords.share.Share.update(ShareCoursePhotoActivity.this,
													filePath, shareText.getText().toString(), "",
													"", shareHandler);
										}
									}
									
								}).start();
						
					}
					else
					{
						showToast( "内容不能为空，并且不能超过140个汉字。" );
					}
				}
			}
			//分享成功后关闭当前页面
			if (msg.obj.equals(ShareHandler.SEND_SUCESS_EN))
			{	
				showToast("分享成功");
				PhotoModel.getInstance(ShareCoursePhotoActivity.this).setList_uploadPhotoBean(null);
				finish();
			}
		}

	};
}
