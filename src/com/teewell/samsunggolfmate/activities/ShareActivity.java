package com.teewell.samsunggolfmate.activities;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Message;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.teewell.darkwarlords.share.AuthorizedOperation;
import com.teewell.darkwarlords.share.utils.ShareHandler;
import com.teewell.samsunggolfmate.beans.UserScoreCardObject;
import com.teewell.samsunggolfmate.common.Contexts;
import com.teewell.samsunggolfmate.costants.KeyValuesConstant;
import com.teewell.samsunggolfmate.utils.BitmapUtil;

public class ShareActivity extends ParentActivity implements OnClickListener {
//	private static final String TAG = "Share";

	private String filePath = null;
	private int inittitle = 0;
	private EditText shareText = null;
	private Button shareBtn = null;
	private Button btn_cancellation;
	// private Button endSessionBtn = null;
	private ImageView deleteImgBtn = null;
	private ImageView imageID = null;
	private TextView screenNameT = null;

	private TextView contentLen = null;
	private LinearLayout clearContent = null;
	private FrameLayout flPic = null;

//	private MyDataBaseAdapter db;

//	private int screenheight;
//	private int screenwidth;
	private UserScoreCardObject scoreCardObject;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.share);
//		db = DataBaseSingle.getInstance(this).getDataBase();
		DisplayMetrics dm = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(dm);
//		screenheight = dm.widthPixels;
//		screenwidth = dm.heightPixels;

		scoreCardObject = (UserScoreCardObject) getIntent()
				.getSerializableExtra(
						KeyValuesConstant.INTENT_USERSCORECARDOBJECT);
		btn_cancellation = (Button) findViewById(R.id.btn_back);
		btn_cancellation.setText(R.string.cancellation);
		shareText = (EditText) findViewById(R.id.shareText);
		shareBtn = (Button) findViewById(R.id.btn_rightTitle);
		shareBtn.setText(getString(R.string.share).toString());
		shareText.setText("今天在@"
				+ scoreCardObject.getGeneralScoreBean().getCourse_Name()
				+ "打球,总杆数"
				+ scoreCardObject.getGeneralScoreBean().getMySelfScore() + ".@"
				+ getString(R.string.app_name));

		deleteImgBtn = (ImageView) findViewById(R.id.deleteImgBtn);
		imageID = (ImageView) findViewById(R.id.imageID);
		screenNameT = (TextView) findViewById(R.id.tv_titleName);
		flPic = (FrameLayout) findViewById(R.id.flPic);
		contentLen = (TextView) findViewById(R.id.contentLen);
		clearContent = (LinearLayout) findViewById(R.id.clearContent);

		shareBtn.setOnClickListener(this);
		btn_cancellation.setOnClickListener(this);
		deleteImgBtn.setOnClickListener(this);
		clearContent.setOnClickListener(this);
		imageID.setOnClickListener(this);
		initPhoto();

		// 先通过isSessionValid方法，把本地文件中保存的授权拿出来
		boolean isSession = AuthorizedOperation
				.isSessionValid(ShareActivity.this);

		//授权是否可用
		if (isSession == true)
		{
//			String screenName;
//			
//			screenName = com.teewell.darkwarlords.share.Share.getScreenName(ShareCoursePhotoActivity.this);
//			if (!TextUtils.isEmpty(screenName))
//			{
//				screenNameT.setText(screenName);
//			}
//			else
//			{
//				screenNameT.setText("已登录");
//			}
			new Thread(new Runnable() {
				
				@Override
				public void run() {
					// TODO Auto-generated method stub
					String screenName = com.teewell.darkwarlords.share.Share.getScreenName(ShareActivity.this);
					
					Message msg = new Message();
					msg.obj= GETSCREENNAME;
					Bundle bundle = new Bundle();
					bundle.putString(GETSCREENNAME, screenName);
					msg.setData(bundle);
					msg.what = inittitle;
					shareHandler.sendMessage(msg);
				}
			}).start();
			btn_cancellation.setVisibility(View.VISIBLE);
		} else {
			screenNameT.setText("未登录");
			btn_cancellation.setVisibility(View.GONE);
		}
	}

	private Bitmap initPhoto() {
		flPic.setVisibility(View.VISIBLE);
		filePath = Contexts.SHARE_SCORECARD_PATH_NAME;
		Bitmap rebitmap = BitmapUtil
				.readBitMap(Contexts.SHARE_SCORECARD_PATH_NAME);
		imageID.setImageBitmap(rebitmap);

		return rebitmap;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		// 授权+分享
		case R.id.btn_rightTitle:
			if (!"".equals(shareText.getText().toString())
					|| (imageID != null && imageID.getDrawable() != null)) {
				AuthorizedOperation.requestAccestoken(ShareActivity.this,
						shareHandler);
			}
			break;
//		 注销
		 case R.id.btn_back:
		 AuthorizedOperation.endSession(ShareActivity.this);
		 screenNameT.setText("未登录");
		 showToast( "已注销" );
		 btn_cancellation.setVisibility(View.GONE);
		 break;
		// 删除图片
		case R.id.deleteImgBtn: {
			final Builder dialog = new AlertDialog.Builder(this);
			dialog.setTitle(getString(R.string.apk_title));
			dialog.setMessage(getString(R.string.del_pic));
			dialog.setPositiveButton(getString(R.string.ok),
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
							flPic.setVisibility(View.GONE);
							filePath = null;
							imageID.setImageBitmap(null);
						}
					});
			dialog.setNegativeButton(getString(R.string.return_),
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
						}
					});
			dialog.create().show();
		}
			break;
		// 删除微博内容
		case R.id.clearContent: {
			final Builder dialog = new AlertDialog.Builder(this);
			dialog.setTitle(getString(R.string.apk_title));
			dialog.setMessage(getString(R.string.delete_all));
			dialog.setPositiveButton(getString(R.string.ok),
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
							shareText.setText("");
							contentLen.setText("140");
							contentLen.setTextColor(R.color.text_num_gray);
						}
					});
			dialog.setNegativeButton(getString(R.string.return_),
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
						}
					});
			dialog.create().show();
		}
			break;
		// 浏览图片
		case R.id.imageID:
			Intent intent = new Intent(ShareActivity.this,
					SimplePhotoShowActivity.class);
			intent.putExtra(SimplePhotoShowActivity.EXTRA_PIC_URI, filePath);
			intent.putExtra(SimplePhotoShowActivity.IS_SCORECARD, true);
			startActivity(intent);
			break;
		}
	}
	public String GETSCREENNAME = "getscreenname";
	private int share =1;
	private ShareHandler shareHandler = new ShareHandler() {
		@Override
		public void handleMessage(Message msg) {
			//授权成功后执行分享
			if (msg.obj.equals(ShareHandler.AUTH_SUCESS_EN))
			{
				new Thread(new Runnable() {
					
					@Override
					public void run() {
						// TODO Auto-generated method stub
						String screenName = com.teewell.darkwarlords.share.Share.getScreenName(ShareActivity.this);
						
						Message msg = new Message();
						msg.obj= GETSCREENNAME;
						msg.what = share;
						Bundle bundle = new Bundle();
						bundle.putString(GETSCREENNAME, screenName);
						msg.setData(bundle);
						shareHandler.sendMessage(msg);
					}
				}).start();
				
			}
			if (msg.obj.equals(GETSCREENNAME)) {
				String screenName = msg.getData().getString(GETSCREENNAME);
				if (!TextUtils.isEmpty(screenName))
				{
					screenNameT.setText(screenName);
				}
				else
				{
					screenNameT.setText("加载失败...");
				}
				if(msg.what == ShareActivity.this.share){
					//判断内容长度是否满足条件
					if (com.teewell.darkwarlords.share.Share.isMeetTheContent(shareText.getText().toString()))
					{
						new Thread(
								new Runnable(){
									@Override
									public void run() {
										if (TextUtils.isEmpty(filePath))
										{
											com.teewell.darkwarlords.share.Share.update(ShareActivity.this,
													"", shareText.getText().toString(), "", "",
													shareHandler);
										}
										else
										{
											com.teewell.darkwarlords.share.Share.update(ShareActivity.this,
													filePath, shareText.getText().toString(), "",
													"", shareHandler);
										}
									}
									
								}).start();
						
					}
					else
					{
						showToast( "内容不能为空，并且不能超过140个汉字。" );
					}
				}
			}
			// 分享成功后关闭当前页面
			if (msg.obj.equals(ShareHandler.SEND_SUCESS_EN)) {
				showToast("分享成功");
				finish();
			}
		}

	};

}
