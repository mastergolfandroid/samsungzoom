package com.teewell.samsunggolfmate.utils;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.http.HttpRequest;
import org.apache.http.client.HttpClient;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;

import android.os.AsyncTask;
import android.util.Log;

/**
 * @Project com.vehiclesApp
 * @package com.vehiclesApp.utils
 * @title AsyncHttpClient.java
 * @Description TODO
 * @author DarkWarlords
 * @date 2012-11-13 下午3:42:32
 * @Copyright Copyright(C) 2012-11-13
 * @version 1.0.0
 */
public class AsyncHttpClient
{
	private static final String TAG = AsyncHttpClient.class.getName();
	private static DefaultHttpClient httpClient;
	
//	public static int CONNECTION_TIMEOUT = 2 * 60 * 1000;
//	public static int SOCKET_TIMEOUT = 2 * 60 * 1000;
	public static int CONNECTION_TIMEOUT = 20 * 1000;
	public static int SOCKET_TIMEOUT = 20 * 1000;
	private static ConcurrentHashMap<Object, AsyncHttpSender> tasks = new ConcurrentHashMap<Object, AsyncHttpSender>();
	public static void sendRequest ( final Object currentActitity,
		final HttpRequest request, AsyncResponseListener callback )
	{
		sendRequest(currentActitity, request, callback, CONNECTION_TIMEOUT,
				SOCKET_TIMEOUT);
	}
	public static void sendRequest ( final Object currentActitity,
		final HttpRequest request, AsyncResponseListener callback,
		int timeoutConnection, int timeoutSocket )
	{
		InputHolder input = new InputHolder(request, callback);
		AsyncHttpSender sender = new AsyncHttpSender();
		sender.execute(input);
		tasks.put(currentActitity, sender);
	}

	public static void cancelRequest ( final Object currentActitity )
	{
		if (tasks == null || tasks.size() == 0)
			return;
		for (Object key : tasks.keySet())
		{
			if (currentActitity == key)
			{
				AsyncTask<?, ?, ?> task = tasks.get(key);
				if (task.getStatus() != null && task.getStatus() != AsyncTask.Status.FINISHED)
				{
					Log.i(TAG, "AsyncTask of " + task + " cancelled.");
					task.cancel(true);
				}
				tasks.remove(key);
			}
		}
	}
	
	public static void cancelAllRequest ( )
	{
		if (tasks == null || tasks.size() == 0)
			return;
		for (Object key : tasks.keySet())
		{
			AsyncTask<?, ?, ?> task = tasks.get(key);
			if (task.getStatus() != null && task.getStatus() != AsyncTask.Status.FINISHED)
			{
				Log.i(TAG, "AsyncTask of " + task + " cancelled.");
				task.cancel(true);
			}
		}
		
		tasks.clear();
	}

	public static void cancelWildRequest ( String wildCard)
	{
		if (tasks == null || tasks.size() == 0)
			return;
		for (Object key : tasks.keySet())
		{
			if(key.getClass().getSimpleName() != String.class.getSimpleName()){
				continue;
			}
			String keyString = (String)key;
			if(keyString.startsWith(wildCard)){
				AsyncTask<?, ?, ?> task = tasks.get(key);
				if (task.getStatus() != null && task.getStatus() != AsyncTask.Status.FINISHED)
				{
					Log.i(TAG, "AsyncTask of " + task + " cancelled.");
					task.cancel(true);
				}
				tasks.remove(key);
			}
		}
	}

	
	public static synchronized HttpClient getClient ()
	{
		if (httpClient == null)
		{
			//use following code to solve Adapter is detached error
			//refer: http://stackoverflow.com/questions/5317882/android-handling-back-button-during-asynctask
			BasicHttpParams params = new BasicHttpParams();
			SchemeRegistry schemeRegistry = new SchemeRegistry();
			schemeRegistry.register(new Scheme("http",
					PlainSocketFactory.getSocketFactory(), 80));
			final SSLSocketFactory sslSocketFactory = SSLSocketFactory.getSocketFactory();
			schemeRegistry.register(new Scheme("https", sslSocketFactory, 443));
			// Set the timeout in milliseconds until a connection is established.
			HttpConnectionParams.setConnectionTimeout(params,
					CONNECTION_TIMEOUT);
			// Set the default socket timeout (SO_TIMEOUT) 
			// in milliseconds which is the timeout for waiting for data.
			HttpConnectionParams.setSoTimeout(params, SOCKET_TIMEOUT);
			ClientConnectionManager cm = new ThreadSafeClientConnManager(
					params, schemeRegistry);
			httpClient = new DefaultHttpClient(cm, params);
		}
		return httpClient;
	}


}
