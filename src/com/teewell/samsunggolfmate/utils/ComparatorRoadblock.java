package com.teewell.samsunggolfmate.utils;

import java.util.Comparator;

import com.teewell.samsunggolfmate.beans.RoadblockObject;


public class ComparatorRoadblock  implements Comparator<RoadblockObject>{
	@Override
	public int compare(RoadblockObject object1, RoadblockObject object2) {
		float result = (object1.getLength_backRoadblockToGreen() - object2.getLength_backRoadblockToGreen());
		if (result>0) {
			return 1;
		}else if(result<0){
			return -1;
		}else {
			return 0;
		}
	}
}

