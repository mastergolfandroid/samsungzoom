package com.teewell.samsunggolfmate.utils;

import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

/**
 * @author Antony
 * 
 */
public class GpsUtil{
    private static final String TAG = GpsUtil.class.getSimpleName();

    public double latitude = 0.0;
    public double longitude = 0.0;
    private static final long GPS_REFRESH_TIME = 1500;
    private static final float GPS_REFRESH_DISTANCE = 0.3f;
    private Handler locationHandler;
	private int locationSignal;

    private LocationManager locationManager;

    public void stopGps() {
        locationManager.removeUpdates(locationListener);
    }

    public void startGps() {
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, GPS_REFRESH_TIME, GPS_REFRESH_DISTANCE, locationListener);
    }

    public GpsUtil(Context context) {
    	Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_FINE);
        criteria.setPowerRequirement(Criteria.POWER_MEDIUM);
        criteria.setAltitudeRequired(false);
        criteria.setSpeedRequired(false);
        criteria.setCostAllowed(true);
        locationManager = (LocationManager) context
                .getSystemService(Context.LOCATION_SERVICE);
        Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        if (location != null) {
            latitude = location.getLatitude();
            longitude = location.getLongitude();
        }
    }
    private final LocationListener locationListener = new LocationListener() {

        public void onStatusChanged(String provider, int status, Bundle extras) {
        }

        public void onProviderEnabled(String provider) {
        }

        public void onProviderDisabled(String provider) {
        }

        public void onLocationChanged(Location location) {
            Log.d(TAG, "get location = " + location);
            if (location != null && locationHandler != null) {
            	latitude = location.getLatitude();
            	longitude = location.getLongitude();
            	locationHandler.sendEmptyMessage(locationSignal);
            }

        }
    };

    private static GpsUtil mGpsUtil;

    public static GpsUtil getInstance(Context context) {
        if (mGpsUtil == null) {
            mGpsUtil = new GpsUtil(context);
        }
        return mGpsUtil;
    }

	public void setHandlerInfo(Handler myHandler,int what_Gps,int accuracy){

		locationHandler = myHandler;
		locationSignal = what_Gps;
		
		startGps();
	}
	
	public void resetHandlerInfo(Handler myHandler,int what_Gps){

		locationHandler = myHandler;
		locationSignal = what_Gps;
		
	}
	public void clearHandlerInfo(){
		locationHandler = null;
		stopGps();
	}

	public void destroy(){
		locationHandler = null;
		stopGps();
        locationManager = null;
        mGpsUtil = null;
	}

}
