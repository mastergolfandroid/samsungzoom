package com.teewell.samsunggolfmate.utils;

/**
 * ֱ�������
 * @author DarkWarlords
 *
 */
public class RightTriangleUtil {
	@SuppressWarnings("unused")
	private final static String TAG = RightTriangleUtil.class.getName() ;
	
	/**
	 * ��֪һ����ǣ�����ʱת��Ϊ���ȣ���б�ߣ�����ǶԱߵı߳����ǵĶԱߣ�
	 * @param hypotenuse	б�߳���
	 * @param acuteAngle	���(�Ƕ�)
	 * @return oppositeSide	�Ա߱߳�
	 */
	public static float getOppositeSideLenth ( float hypotenuse , float acuteAngle )
	{
		double sinA = Math.sin( convertAngle2Radian(acuteAngle) );
		return (float) (sinA * hypotenuse);
	}
	
	/**
	 * ��֪һ����ǣ�����ʱת��Ϊ���ȣ���б�ߣ�������ٱߵı߳� ���ǵ��ٱߣ�
	 * @param hypotenuse	б�߳���
	 * @param acuteAngle	���(�Ƕ�)
	 * @return adjacentSide �ٽǱ߱߳�
	 */
	public static float getNearSideLenth ( float hypotenuse , float acuteAngle )
	{
		double cosA = Math.cos( convertAngle2Radian(acuteAngle) );
		return (float) (cosA * hypotenuse);
	}
	
	/**
	 * ��֪�ٱߺ�б�߱߳��������Ӧ����ǵĽǶ�
	 * @param adjacentSide 		����ٱߵı߳�
	 * @param hypotenuse 		б�ߵı߳�
	 * @return acuteAngle 		��ǽǶ�
	 */
	public static float getAcuteAngle(float adjacentSide , float hypotenuse)
	{
    	float radian = (float) Math.acos(adjacentSide/hypotenuse);				//������
    	return (float) convertRadian2Angle(radian) ;		//������ת��Ϊ�Ƕ�
	}
	
	/**
	 * ���Ƕ�ת��Ϊ���ȣ�������Ǻ�����㣩这两个函数中的X 都是指的“弧度”而非“角度”，弧度的计算公式为： 2*PI/360*角度；
	 * @param angle	�Ƕ�
	 * @return radian ����
	 */
	public static double convertAngle2Radian(float angle)
	{
		return angle * Math.PI / 180;
	}
	
	/**
	 * ������ת��Ϊ�Ƕȣ�������Ǻ�����㣩
	 * @param radian ����
	 * @return angle	�Ƕ�
	 */
	public static double convertRadian2Angle(float radian)
	{
		return radian * 180 / Math.PI ;
	}
}