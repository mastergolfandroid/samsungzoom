package com.teewell.samsunggolfmate.utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import android.util.Log;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

/**  
 * @Project com.vehiclesApp
 * @package com.vehiclesApp.utils
 * @title JsonProcessUtil.java 
 * @Description 负责Json字符串与对象之间的转换及相关处理
 * @author teewell
 * @date Nov 14, 2012 4:20:50 PM
 * @Copyright Copyright(C) Nov 14, 2012
 * @version 1.0.0
 */
public class JsonProcessUtil {
	private static final String TAG = "JsonProcessUtil";
	private JsonProcessUtil(){
		
	}
	
	private static ObjectMapper objectMapper = null;
	public static ObjectMapper getMapperInstance(){
		if (null == objectMapper) {
			objectMapper = new ObjectMapper();
		}
		
		return objectMapper;
	}
	
	/**
	 * 将对象转换为Json字符串
	 * @param obj
	 * @return
	 */
	public static String toJSON(Object obj) {
		
		StringWriter writer = new StringWriter();
		ObjectMapper objectMapper = getMapperInstance();
		try{
			objectMapper.writeValue(writer, obj);
		}catch (JsonGenerationException e) {
			// TODO: handle exception
			Log.i(TAG, e.toString());
		}catch (JsonMappingException e) {
			// TODO: handle exception
			Log.i(TAG, e.toString());
		}catch (IOException e) {
			// TODO: handle exception
			Log.i(TAG, e.toString());
		}
		
		return writer.toString();
	}

	/**
	 * 将json字符串转换为指定的对象
	 * @param json 字符串
	 * @param clazz 指定的类
	 * @return
	 */
	public static Object fromJSON(String json, Class<?> clazz) {
		ObjectMapper objectMapper = getMapperInstance();
		try{
			return objectMapper.readValue(json, clazz);
		}catch (JsonGenerationException e) {
			// TODO: handle exception
			Log.i(TAG, e.toString());
		}catch (JsonMappingException e) {
			// TODO: handle exception
			Log.i(TAG, e.toString());
		}catch (IOException e) {
			// TODO: handle exception
			Log.i(TAG, e.toString());
		}
		return null;
	}
	
	/**
	 * 将json字符串转换为对象数组
	 * 这里使用泛型操作
	 * @param json 字符串 
	 * @param clazz 指定的类
	 * @return
	 */
	public static <T> ArrayList<T> arrayListFromJSON(String json, Class<T> clazz) {
		ObjectMapper objectMapper = getMapperInstance();
		try{
			ArrayList<T> retArrayList = new ArrayList<T>();
			
			//第一步转换为JsonNode数组
			JsonNode[] nodes = objectMapper.readValue(json, JsonNode[].class);
			Log.i(TAG, json);
			
			//第二步，分别转换为指定的类对象，然后添加到list中
			for (JsonNode node : nodes) {
				retArrayList.add(objectMapper.readValue(node.toString(), clazz));
			}
			
			return retArrayList;
		}catch (JsonGenerationException e) {
			// TODO: handle exception
			Log.i(TAG, e.toString());
		}catch (JsonMappingException e) {
			// TODO: handle exception
			Log.i(TAG, e.toString());
		}catch (IOException e) {
			// TODO: handle exception
			Log.i(TAG, e.toString());
		}
		
		return null;
	}

	/**
	 * 将指定的inputStream转换为对象list
	 * @param jsonInputStream inputstream
	 * @param clazz 指定的类
	 * @return
	 */
	public static <T> ArrayList<T> arrayListFromInputStream(InputStream jsonInputStream, Class<T> clazz) {
		ObjectMapper objectMapper = getMapperInstance();
		try{
			
			ArrayList<T> retArrayList = new ArrayList<T>();
			JsonNode[] nodes = objectMapper.readValue(jsonInputStream, JsonNode[].class);
			for (JsonNode node : nodes) {
				retArrayList.add(objectMapper.readValue(node.toString(), clazz));
			}
			
			return retArrayList;
		}catch (JsonGenerationException e) {
			// TODO: handle exception
			Log.i(TAG, e.toString());
		}catch (JsonMappingException e) {
			// TODO: handle exception
			Log.i(TAG, e.toString());
		}catch (IOException e) {
			// TODO: handle exception
			Log.i(TAG, e.toString());
		}
		
		return null;
	}
	
	/**
	 * 将对象list转换为，Json字符串
	 * @param arrayList 对象List
	 * @return Json字符串
	 */
	public static <T> String arrayListToJSON(ArrayList<T> arrayList) {
		ObjectMapper objectMapper = getMapperInstance();
		StringWriter writer = new StringWriter();
		try{
			objectMapper.writeValue(writer, arrayList);
		}catch (JsonGenerationException e) {
			// TODO: handle exception
			throw new RuntimeException(e);
		}catch (JsonMappingException e) {
			// TODO: handle exception
			throw new RuntimeException(e);
		}catch (IOException e) {
			// TODO: handle exception
			throw new RuntimeException(e);
		}
		
		return writer.toString();
	}

	/**
	 * 找到Json字符串中，指定的项的字符串值
	 * @param json 原始字符串
	 * @param valueName 字符串关键字
	 * @return 子项字符串
	 */
	public static String searchString(String json,String valueName){
		if( json==null || valueName==null ){
			return null;
		}
		String value = null;
		try {
			@SuppressWarnings("unchecked")
			Map<String, String> map = (HashMap<String, String>)(objectMapper.readValue(json, HashMap.class));
			value = map.get(valueName);
		} catch (Exception e) {
			Log.e(TAG, "Read " + valueName + " for json error.");
		}
		return value;
	}

	/**
	 * 找到Json字符串中，指定的项的整数值
	 * @param json 原始字符串
	 * @param valueName 字符串关键字
	 * @return 子项整形值
	 */
	public static int searchInt(String json,String valueName){
		int value = 0;
		if( json==null || valueName==null ){
			return value;
		}
		try {
			@SuppressWarnings("unchecked")
			Map<String,Integer> map = (HashMap<String, Integer>)(objectMapper.readValue(json, HashMap.class));
			value = map.get(valueName);
		} catch (Exception e) {
			Log.e(TAG, "Read " + valueName + " for json error.");
		}
		return value;
	}
}
