package com.teewell.samsunggolfmate.utils;

import java.util.HashMap;
import java.util.Map;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.Request.Method;
import com.android.volley.RequestQueue;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.teewell.samsunggolfmate.beans.BitmapLruCache;


/**
 * Helper class that is used to provide references to initialized RequestQueue(s) and ImageLoader(s)
 * 
 * @author Ognyan Bankov
 * 
 */
public class MyVolley {
    private static final int MAX_IMAGE_CACHE_ENTIRES  = 10000;
    
    private static RequestQueue mRequestQueue;
    private static ImageLoader mImageLoader;


    private MyVolley() {
        // no instances
    }


   public static void init(Context context) {
        mRequestQueue = Volley.newRequestQueue(context);
        mImageLoader = new ImageLoader(mRequestQueue, new BitmapLruCache(MAX_IMAGE_CACHE_ENTIRES));
    }


    public static RequestQueue getRequestQueue() {
        if (mRequestQueue != null) {
            return mRequestQueue;
        } else {
            throw new IllegalStateException("RequestQueue not initialized");
        }
    }
    public static void Post(String url,Listener<String> listener,ErrorListener errorListener,final HashMap<String, String> params){
    	StringRequest myReq = new StringRequest(Method.POST,
				url,
				listener, errorListener) {
			@Override
			protected Map<String, String> getParams() throws AuthFailureError {
				// TODO Auto-generated method stub
				return params;

			}
			
		};
		mRequestQueue.add(myReq);
    }
    
    public static void Post(String url,Listener<String> listener,ErrorListener errorListener){
    	StringRequest myReq = new StringRequest(Method.POST,
				url,
				listener, errorListener);
		mRequestQueue.add(myReq);
    }
    public static void Get(String url,Listener<String> listener,ErrorListener errorListener){
    	StringRequest myReq = new StringRequest(Method.GET,
				url,
				listener, errorListener);
		MyVolley.getImageLoader();
		mRequestQueue.add(myReq);
    }
    /**
     * Returns instance of ImageLoader initialized with {@see FakeImageCache} which effectively means
     * that no memory caching is used. This is useful for images that you know that will be show
     * only once.
     * 
     * @return
     */
    public static ImageLoader getImageLoader() {
        if (mImageLoader != null) {
            return mImageLoader;
        } else {
            throw new IllegalStateException("ImageLoader not initialized");
        }
    }
}
