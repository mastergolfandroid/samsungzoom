package com.teewell.samsunggolfmate.utils;

import org.apache.http.HttpEntity;

/**  
 * @Project com.vehiclesApp
 * @package com.vehiclesApp.utils
 * @title AsyncResponseListener.java 
 * @Description TODO
 * @author DarkWarlords
 * @date 2012-11-13 下午3:50:05
 * @Copyright Copyright(C) 2012-11-13
 * @version 1.0.0
 */
public interface AsyncResponseListener {
	/** Handle successful response */
	public void onResponseReceived(HttpEntity response);
	
	/** Handle exception */
	public void onResponseReceived(Throwable response);
}
