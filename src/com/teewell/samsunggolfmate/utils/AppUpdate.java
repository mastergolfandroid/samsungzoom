package com.teewell.samsunggolfmate.utils;
import android.app.Application;
import android.content.Context;
import android.util.Log;

/**  
 * @Project com.vehiclesApp
 * @package com.vehiclesApp.utils
 * @title AppUpdate.java 
 * @Description 检测软件是否需要更新
 * @author DarkWarlords
 * @date 2013-1-6 上午11:10:25
 * @Copyright Copyright(C) 2013-1-6
 * @version 1.0.0
 */
public class AppUpdate extends Application {
	
	private static final String TAG = AppUpdate.class.getName();
	private static final AppUpdate INSTANCE = new AppUpdate();
	private Context context;
	
	private AppUpdate(){}
	public static AppUpdate getInstance(Context context){
		INSTANCE.context = context;
		return INSTANCE;
	}
	
	/**
	 * 判断客户端是否有更新，如果有提示确认更新对话框
	 */
	@SuppressWarnings("unused")
	public void checkUpdate() {
		try {
			int versionCode = context.getApplicationContext().getPackageManager()
					.getPackageInfo("com.vehiclesApp.activities", 0).versionCode;
			String versionName = context.getApplicationContext().getPackageManager()
					.getPackageInfo("com.vehiclesApp.activities", 0).versionName;
//			AsyncHttpClient.sendRequest((Activity) context, AppUpdateModel.checkVersion(versionCode, versionName) ,listener );
		} catch (Exception e) {
			Log.e(TAG, "Failed to get app version code");
		}
	}
	
	AbstractAsyncResponseListener listener = new AbstractAsyncResponseListener() {
		@Override
		protected void onSuccess(String response) {
//			AppUpdateBean appUpdateBean = (AppUpdateBean) JsonProcessUtil.fromJSON(response , AppUpdateBean.class); 
//			if( "1".equals(appUpdateBean.getResultCode())  ){	//0不需要更新，1需要更新
//				showDownloadDialog(appUpdateBean.getDownloadUrl());
//			}
		}
		@Override
		protected void onFailure(Throwable e) {
			Log.e(TAG, "查询客户端是否需要更新的请求失败！");
		}
	};
	
//	private void showDownloadDialog( final String url ) {  
//		Dialog openWifiDialog = new AlertDialog.Builder(context)
//		.setTitle(context.getString(R.string.title_app_version_update).toString())
//		.setMessage(context.getResources().getString(R.string.prompt_app_version_update).toString())
//		.setPositiveButton(context.getString(R.string.ok).toString(), new OnClickListener() {
//			@Override
//			public void onClick(DialogInterface dialog, int which) {
//				dialog.dismiss();
//				Intent viewIntent = new 
//				Intent(Intent.ACTION_VIEW,Uri.parse(url));
//				context.startActivity(viewIntent);
//			}
//		})
//		.setNegativeButton(context.getString(R.string.cancel).toString(), new OnClickListener() {
//			public void onClick(DialogInterface dialog, int which) {
//				dialog.dismiss();
//			}
//		}).create();
//		openWifiDialog.show();
//		openWifiDialog.setCancelable(false);
//	}
}
