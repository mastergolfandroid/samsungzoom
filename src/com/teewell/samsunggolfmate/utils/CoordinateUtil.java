package com.teewell.samsunggolfmate.utils;
import com.teewell.samsunggolfmate.beans.Point;

import android.util.Log;

/**
 * 经纬度和屏幕坐标转换的工具类。原理是通过图片的左上角、右下角经纬度，
 * 结合坐标系旋转、平移公式。图片绕中心点旋转前后，坐标不变。
 * @author Antony
 * 
 */
public class CoordinateUtil {
    /**
     * 旋转角度
     */
    private float rotateAngle; // 旋转角度
    private double latLTop; //图片左上角纬度
    private double lonLTop; //图片左上角经度
    private double latRBottom;//图片左下角纬度
    private double lonRBottom;//图片左下角经度
    private float midpointX; // 图片中心点X坐标
    private float midpointY; // 图片中心点Y坐标
    private float bitmapStartY;
    private float bitmapStartX;
    private float viewWidth;
    private float viewHeight;
    /**
     * 一米对应多少像素
     */
    private float oneMeterToPixel;
    public float getViewWidth(){
        return viewWidth;
    }
    public float getViewHeight(){
        return viewHeight;
    }
    

	public void setViewWidth(float viewWidth) {
		this.viewWidth = viewWidth;
	}
	public void setViewHeight(float viewHeight) {
		this.viewHeight = viewHeight;
	}
	public float getOneMeterToPixel() {
		return oneMeterToPixel;
	}
	public void setOneMeterToPixel(float oneMeterToPixel) {
		this.oneMeterToPixel = oneMeterToPixel;
	}
    /**
     * @return the rotateAngle
     */
    public float getRotateAngle() {
        return rotateAngle;
    }

    /**
     * @return the latLTop
     */
    public double getLatLTop() {
        return latLTop;
    }

    /**
     * @param latLTop the latLTop to set
     */
    public void setLatLTop(double latLTop) {
        this.latLTop = latLTop;
    }

    /**
     * @return the lonLTop
     */
    public double getLonLTop() {
        return lonLTop;
    }

    /**
     * @param lonLTop the lonLTop to set
     */
    public void setLonLTop(double lonLTop) {
        this.lonLTop = lonLTop;
    }

    /**
     * @return the latRBottom
     */
    public double getLatRBottom() {
        return latRBottom;
    }

    /**
     * @param latRBottom the latRBottom to set
     */
    public void setLatRBottom(double latRBottom) {
        this.latRBottom = latRBottom;
    }

    /**
     * @return the lonRBottom
     */
    public double getLonRBottom() {
        return lonRBottom;
    }

    /**
     * @param lonRBottom the lonRBottom to set
     */
    public void setLonRBottom(double lonRBottom) {
        this.lonRBottom = lonRBottom;
    }

    /**
     * @param rotateAngle the rotateAngle to set
     */
    public void setRotateAngle(float rotateAngle) {
        this.rotateAngle = rotateAngle;
    }

    /**
     * @return the bmpWidth
     */
    public float getBmpWidth() {
        return bmpWidth;
    }

    /**
     * @param bmpWidth the bmpWidth to set
     */
    public void setBmpWidth(float bmpWidth) {
        this.bmpWidth = bmpWidth;
    }

    /**
     * @return the bmpHeight
     */
    public float getBmpHeight() {
        return bmpHeight;
    }

    /**
     * @param bmpHeight the bmpHeight to set
     */
    public void setBmpHeight(float bmpHeight) {
        this.bmpHeight = bmpHeight;
    }

    public float bmpWidth;
    public float bmpHeight;


    /**
     * 图片上一像素表示多少经度
     */
    private double onePxToLat;
    /**
     * 图片上一像素表示多少纬度
     */
    private double onePxToLon;
    
    private int[] validArea;
    private float ratio;

    
    /**
     * @param density 屏幕密度
     * @param validArea 有效区域的x1 y1, x2 y2坐标
     * @param bitmapStartY 图片在原图放大或缩小后的Y坐标位置
     * @param rotateAngle
     *            旋转的角度
     * @param width
     *            图片的宽
     * @param height
     *            图片的高
     * @param lonLTop
     *            图片左上角的经度
     * @param latLTop
     *            图片左上角的纬度
     * @param lonRBottom
     *            图片右下角的经度
     * @param latRBottom
     *            图片右下角的纬度

     */
    public CoordinateUtil(int[] validArea, float rotateAngle, double lonLTop, double latLTop,
            double lonRBottom, double latRBottom) {
    	this.rotateAngle = rotateAngle;
    	this.validArea = validArea;
        this.lonLTop = lonLTop;
        this.latLTop = latLTop;
        this.lonRBottom = lonRBottom;
        this.latRBottom = latRBottom;
    }
    public void resetAttribute(float ratio,  float bitmapX, float bitmapY,float bmpWidth, float bmpHeight){
    	 this.ratio = ratio;
         
         this.bitmapStartY = bitmapY;
         this.bitmapStartX = bitmapX;
         
         this.bmpWidth = bmpWidth;
         this.bmpHeight = bmpHeight;
         
         reset();
    }
    /**
     * 重新计算屏幕偏移量、终点坐标等
     */
    public void reset(){
        
        onePxToLon = Math.abs((lonRBottom - lonLTop) / bmpWidth);
        onePxToLat = Math.abs((latRBottom - latLTop) / bmpHeight);
         
        oneMeterToPixel = (float) Math.sqrt(bmpWidth*bmpWidth+bmpHeight*bmpHeight)/DistanceUtil.distanceBetween(latLTop, lonLTop, latRBottom, lonRBottom);
       
        calculateMidpoint();
    }


    /**
     * 计算图片的中心点坐标
     */
    private void calculateMidpoint() {
        midpointX = bmpWidth / 2;
        midpointY = -bmpHeight / 2;
    }
    

    /**
     * 旋转后点击的点的坐标转换为经纬度
     * 
     * @param touchX
     *            点击的点在屏幕上的X坐标
     * @param touchY
     *            点击的点在屏幕上的Y坐标
     * @return
     * double[0] longitude
     * double[1] latitude
     */
    public Point getLonLatByTouch(float touchX, float touchY) {
    	 Point point = new Point();
    	  float bitmapTouchX = touchX - this.bitmapStartX;
          float bitmapTouchY = -touchY + this.bitmapStartY;
      	double newX = midpointX + (bitmapTouchX - midpointX)*Math.cos(rotateAngle * 2*Math.PI / 360)-(bitmapTouchY-midpointY)*Math.sin(rotateAngle * 2*Math.PI / 360);
      	double newY = midpointY + (bitmapTouchX - midpointX)*Math.sin(rotateAngle * 2*Math.PI / 360)+(bitmapTouchY-midpointY)*Math.cos(rotateAngle * 2*Math.PI / 360);
      	newY = -newY;
          point.lon = newX * onePxToLon + lonLTop;
          point.lat = latLTop - newY * onePxToLat;
          point.x = touchX;
          point.y = touchY;
        return point;
    }

    /**
     * 将经纬度装换为屏幕上的点坐标
     * 
     * @param lon
     *            经度
     * @param lat
     *            纬度
     * @return Point对象
     */
    public Point convertLonLatToOldXY(double lon, double lat) {
        Point point = new Point();

        if (lon>0) {
        	double x1 = (lon - lonLTop) / onePxToLon;
            double y1 = (latLTop - lat) / onePxToLat;
            y1 = -y1;
//            Log.e("----", "rotateAngle="+rotateAngle+",x1=" + x1+",y1=" + y1+",this.bitmapX="+this.bitmapStartX+",this.bitmapY"+this.bitmapStartY);
            double bitmapX = midpointX + (x1 - midpointX)*Math.cos(-rotateAngle * 2*Math.PI / 360)-(y1-midpointY)*Math.sin(-rotateAngle * 2*Math.PI / 360);
            double bitmapY = midpointY + (x1 - midpointX)*Math.sin(-rotateAngle * 2*Math.PI / 360)+(y1-midpointY)*Math.cos(-rotateAngle * 2*Math.PI / 360);
            bitmapY = Math.abs(-bitmapY);
            point.x = (float) (bitmapX + this.bitmapStartX);
            point.y = (float) (bitmapY + this.bitmapStartY);
          
//            Log.e("----", "bitmapX=" + bitmapX+",bitmapY=" + bitmapY);
//            Log.e("----", "point.x=" + point.x+",point.y=" + point.y);
		}       
        point.lon = lon;
        point.lat = lat;
        return point;
    }

   public boolean isValidMovingAera(float x,float y){
	   float x1 =  (validArea[0]*ratio);
	   float y1 =  (validArea[1]*ratio);
	   float x2 =  (validArea[2]*ratio);
	   float y2 =  (validArea[3]*ratio);
	   float minX = viewWidth/2;
	   float minY = viewHeight/2;
//       Log.e("isValidMovingAera", x1+","+y1+","+x2+","+y2+","+x+","+y);
	   boolean flag = true;
	   //通过显示区域来处理
	   float widthDirft = (x2-x1-(validArea[2]-validArea[0]))/2;
	   float heightDirft = (y2-y1-(validArea[3]-validArea[1]))/2;
	   if (x<=(minX-widthDirft)||x>=minX+widthDirft) {
		   return false;
	   }
	   if (y<=minY-heightDirft || y >= minY + heightDirft) {
		   return false;
	   }
	   return flag;
   }

	public int[] getValidArea() {
		return validArea;
	}
	public void setValidArea(int[] validArea) {
		this.validArea = validArea;
	}
    
}
