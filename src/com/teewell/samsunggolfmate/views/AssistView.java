package com.teewell.samsunggolfmate.views;

import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.FloatMath;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import com.teewell.samsunggolfmate.activities.MapActivity;
import com.teewell.samsunggolfmate.activities.R;
import com.teewell.samsunggolfmate.beans.FuncButtonState;
import com.teewell.samsunggolfmate.beans.MapMarkeVo;
import com.teewell.samsunggolfmate.beans.Point;
import com.teewell.samsunggolfmate.common.Contexts;
import com.teewell.samsunggolfmate.common.RountContexts;
import com.teewell.samsunggolfmate.models.MapModel;
import com.teewell.samsunggolfmate.utils.CoordinateUtil;
import com.teewell.samsunggolfmate.utils.DistanceDataUtil;
import com.teewell.samsunggolfmate.utils.DistanceUtil;
import com.teewell.samsunggolfmate.utils.GpsUtil;
import com.teewell.samsunggolfmate.utils.RightTriangleUtil;
import com.teewell.samsunggolfmate.utils.ScreenUtil;
/**
 * 航拍图控件
 * @author chengmingyan
 */
public class AssistView extends View {

    private static final String TAG = AssistView.class.getName();
    private static  float RADIUS;

	/**
     * 左边图片的左起点坐标
     */
    private final int LEFT_BITMAP_LEFT = 10;
    /**
     * 左边果岭旗子的上起点坐标
     */
    private final int LEFT_FlAG_TOP = 15;
    
    private Paint defaultPaint;
    private Paint greenFontPaint;
    private Paint arcLineFontPaint;
    private Paint marketPaint;
    private Paint cursorLinePaint;
    private Paint cursorFontPaint;

    /**
     * 果岭旗子
     */
    private Bitmap flagBmp;
    private Bitmap flagBmp1;
    private Bitmap flagBmp2;
    /**
     * 游标图片
     */
    private Bitmap cursorBmp;

    private Bitmap markBitmap;
    private Bitmap markBitmapDelete;
    private Bitmap markBitmapMoving;

    private MapMarkeVo movingMarkeVo;//当前拖动的marke对象

    @SuppressWarnings("unused")
	private Point touchPoint;
    /**
     * cursor在y轴上的偏移量
     */
    public static float touch_offset_Y = 100;
    
    /**
     * 按钮控件的偏移区域
     */
    private static float image_offset = 40;
    private MapModel mapModel;
    private Paint paint;
    private static float buttonY=60;
    /**
     * 弧线值集合
     */
    private int[] distanceArc;
    private static float cursor_textSize;//弧线字体大小 480的20  720的22
    private static float arc_textSize;//弧线字体大小 480的20  720的22
    private static float mark_textSize;//弧线字体大小 480的20  720的22
    private static float green_textSize;//果岭字体大小 480的20  720的22
    private static float image_bottom_offset=30;//480的30  720的60
	private CoordinateUtil util;
	private int index;
	private String distanceUnit_near;
	private float oneDistanceToPixel;
	private double longitude, latitude;
	private GpsUtil gpsUtil;
	private Handler handler;
    public double getLongitude() {
    	longitude = gpsUtil.longitude;
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	public double getLatitude() {
		latitude = gpsUtil.latitude;
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	
	public AssistView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }
    public AssistView(Context context, AttributeSet attrs) {
    	super(context, attrs);
    	init(context);
    }
   
    private void initBitmap(){
    	markBitmap = BitmapFactory.decodeResource(getResources(),R.drawable.mark);
		markBitmapDelete= BitmapFactory.decodeResource(getResources(),R.drawable.mark_move);
		markBitmapMoving = BitmapFactory.decodeResource(getResources(),R.drawable.mark_remove);
		
		cursorBmp = BitmapFactory.decodeResource(getResources(),R.drawable.cursor);
				
		// 指示果岭的图片
        flagBmp = BitmapFactory.decodeResource(getResources(), R.drawable.flag);
        flagBmp1 = BitmapFactory.decodeResource(getResources(), R.drawable.flag1);
        flagBmp2 = BitmapFactory.decodeResource(getResources(), R.drawable.flag2);
    }
    private void initPaint(){
    	paint = new Paint();
        paint.setStrokeWidth(2f);
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.STROKE);

        touchPoint = new Point();
        cursorLinePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        cursorLinePaint.setStyle(Paint.Style.STROKE);
        cursorLinePaint.setColor(Color.WHITE);
        cursorLinePaint.setStrokeWidth(3f);

        marketPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        marketPaint.setStyle(Paint.Style.STROKE);
        marketPaint.setColor(Color.RED);
        marketPaint.setTextSize(mark_textSize);

        defaultPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        defaultPaint.setStyle(Paint.Style.STROKE);

        greenFontPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        greenFontPaint.setStyle(Paint.Style.STROKE);
        greenFontPaint.setColor(Color.WHITE);
        greenFontPaint.setTextSize(green_textSize);
        
        cursorFontPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        cursorFontPaint.setStyle(Paint.Style.STROKE);
        cursorFontPaint.setColor(Color.WHITE);
        cursorFontPaint.setTextSize(cursor_textSize);
        
        arcLineFontPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        arcLineFontPaint.setColor(Color.WHITE);
        arcLineFontPaint.setTextSize(arc_textSize);
    }
    /**
     * 初始化用到的各种图标和画笔
     */
	private void init(Context contexts) {
		gpsUtil = GpsUtil.getInstance(contexts);
		float density = ScreenUtil.getScreenDensity(contexts);
    	initBitmap();
    	
    	initDistance(contexts);
		
		touch_offset_Y = contexts.getResources().getDimension(R.dimen.cursor_touch_offset_Y)/density;
		image_offset = contexts.getResources().getDimension(R.dimen.image_offset)/density;
		RADIUS = contexts.getResources().getDimension(R.dimen.RADIUS)/density;
		
    	image_bottom_offset = contexts.getResources().getDimension(R.dimen.image_bottom_offset)/density;
		
    	cursor_textSize = contexts.getResources().getDimension(R.dimen.cursor_textSize);
    	mark_textSize = contexts.getResources().getDimension(R.dimen.mark_textSize);
		arc_textSize = contexts.getResources().getDimension(R.dimen.arc_textSize);
		green_textSize = contexts.getResources().getDimension(R.dimen.green_textSize);
    	buttonY = markBitmap.getHeight()+10+image_bottom_offset;
  		
        initPaint();
        
    }
	 /**
	 * 初始化距离单位
	 */
	public  void initDistance(Context contexts){
		index = RountContexts.shortDistanceIndex;
		distanceUnit_near = DistanceUtil.getDistanceUnit(contexts, index);
    	distanceArc = DistanceDataUtil.getDistanceArc(contexts);
    	
    	if (index==DistanceUtil.YARD) {
			oneDistanceToPixel /= DistanceUtil.RATE_YARD;
		}
	 }
    public void setNavigationMapModel(MapModel navigationMapModel,Handler handler) {
        this.mapModel = navigationMapModel;
        this.handler = handler;
        util = (CoordinateUtil) navigationMapModel.getCoordinateUtil();
        oneDistanceToPixel = util.getOneMeterToPixel();
    }
    public MapModel getNavigationMapModel() {
        return mapModel;
    }
    /**
     * @param context
     */
    public AssistView(Context context) {
        super(context);
        init(context);
    }
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (mapModel==null || util == null) {
            canvas.drawColor(Color.BLACK);
            return;
        }
        if (util.getViewWidth()==0) {
			util.setViewWidth(this.getMeasuredWidth());
			util.setViewHeight(this.getMeasuredHeight());
		}
          // 画小人
        float left_c = 0;
        float top_c = 0;
    	
    	left_c = mapModel.getPersonPoint().x;
        top_c = mapModel.getPersonPoint().y;
    	
        // 画果岭小旗
        if(mapModel.getGreenPointArray().size()==1){
        	Point p = mapModel.getGreenPointArray().get(0);
            String myLenToGreen=mapModel.getDistanceToGreen(p,index)+distanceUnit_near;
        	canvas.drawBitmap(flagBmp, LEFT_BITMAP_LEFT, LEFT_FlAG_TOP, defaultPaint);
        	canvas.drawText(myLenToGreen, LEFT_BITMAP_LEFT+flagBmp.getWidth(), LEFT_FlAG_TOP+flagBmp.getHeight(), greenFontPaint);
        }else {
        	int green_count=0;
            for (Point p : mapModel.getGreenPointArray()) {
            	green_count++;
            	 String myLenToGreen=mapModel.getDistanceToGreen(p,index)+distanceUnit_near;
                if (green_count==1) {
                	canvas.drawBitmap(flagBmp1, LEFT_BITMAP_LEFT, LEFT_FlAG_TOP, defaultPaint);
                	canvas.drawText(myLenToGreen, LEFT_BITMAP_LEFT*2+flagBmp1.getWidth(), LEFT_FlAG_TOP+flagBmp1.getHeight(), greenFontPaint);
    			}else {
    				canvas.drawBitmap(flagBmp2, LEFT_BITMAP_LEFT, LEFT_FlAG_TOP+flagBmp2.getHeight(), defaultPaint);
                	canvas.drawText(myLenToGreen, LEFT_BITMAP_LEFT*2+flagBmp2.getWidth(), LEFT_FlAG_TOP+flagBmp2.getHeight()*2, greenFontPaint);
    			}
            }
		}
        
      //在航拍图上画弧线，及到每条弧线的长度
        if (distanceArc!=null) {
        	drawArcAndDistance(canvas, left_c, top_c);
		}

        //draw cursor
        List<MapMarkeVo> voList = mapModel.getMapMarkeVoList();
        switch(mapModel.getFuncButtonState()){
        case cursor:
            drawTouch(canvas);
        	break;
        case addMark:
        	if (voList!=null) {
        		for (int i = 0;i < voList.size();i++) {
            		MapMarkeVo vo = voList.get(i);
            		drawMarke(canvas, vo, markBitmap);
    			}
			}
        	if (movingMarkeVo != null) {
        		drawMarke(canvas, movingMarkeVo, markBitmapDelete);
			}
        	break;
        case removeMark:
        	if (voList!=null) {
        		for (int i = 0;i < voList.size();i++) {
            		MapMarkeVo vo = voList.get(i);
            		drawMarke(canvas, vo, markBitmapMoving);
    			}
			}
        	
        	break;
        default:
        	break;
        }
    }

    private boolean isPersonPosition(){
    	return mapModel.isPersonPositionByLonLat(gpsUtil.longitude,gpsUtil.latitude);
    }
    
    /**在航拍图上画弧线，及到每条弧线的长度
     * @param canvas
     * @param left_c
     * @param top_c
     */
    private void drawArcAndDistance(Canvas canvas, float left_c, float top_c){
        //以小人位置为基点，显示“距离信息”时的偏移角度(使用时需要转换为弧度：*Math.PI/180)
        final int defaultSweepAngle = 90;
        final int defaultStartAngle = 225;
        final int textWidth = 120;
        
        float textSweepAngle = defaultStartAngle - 180;
        
        Path path = new Path();
        //第一道弧线和距离信息
        float maxRadius = distanceArc[distanceArc.length - 1] * oneDistanceToPixel;
        
        float marginToRight = RightTriangleUtil.getNearSideLenth(maxRadius, textSweepAngle);			//临角边边长
        
        //第一次获取“距离信息”在屏幕中弧线上的点位，判断是否超出屏幕范围，如果超出，则修改默认的60度为实际需要的大小
    	Log.v(TAG, "maxRadius" +  maxRadius);
    	Log.v(TAG, "marginToRight:" + marginToRight);
    	Log.v(TAG, "left_c" +  left_c);
        if( marginToRight + textWidth + left_c > ScreenUtil.getScreenWidth(this.getContext()) )
        {
        	marginToRight = ScreenUtil.getScreenWidth(this.getContext()) - left_c - textWidth;
        	textSweepAngle = RightTriangleUtil.getAcuteAngle(marginToRight, maxRadius);				//反算出所需要的角度
        	Log.v(TAG, "angle:" + textSweepAngle);
        }
        //用于计算出外切于圆弧的直线上的两个点的辅助线
        float auxiliaryLineY = RightTriangleUtil.getOppositeSideLenth(textWidth, 90 - textSweepAngle );		//辅助线对边边长
        float auxiliaryLineX = RightTriangleUtil.getNearSideLenth(textWidth, 90 - textSweepAngle);			//辅助线临角边边长
        
        for( int i=0; i<distanceArc.length ;i++ )
        {
        	float radius = distanceArc[i] * oneDistanceToPixel;
        	if( radius < top_c )
            {
        		RectF oval3 = new RectF(left_c - radius, top_c - radius,  left_c + radius, top_c + radius);
                paint.setColor(this.getResources().getColor(R.color.arc));
                canvas.drawArc(oval3, defaultStartAngle, defaultSweepAngle, false, paint);	
                
                //画弧
                float oppositeSide = RightTriangleUtil.getOppositeSideLenth(radius, textSweepAngle);	//对边边长
                float adjacentSide = RightTriangleUtil.getNearSideLenth(radius, textSweepAngle);		//临角边边长
                float distanceTextX = left_c + adjacentSide;
                float distanceTextY = top_c - oppositeSide ;
                
                //虚拟一条穿过这两个点同时外切该圆弧的直线（用于使显示的文字倾斜）
                path.moveTo( distanceTextX, distanceTextY);
                path.lineTo( distanceTextX + auxiliaryLineX, distanceTextY + auxiliaryLineY );
                path.close();
                
                paint.setColor(this.getResources().getColor(R.color.white));
                canvas.drawTextOnPath(distanceArc[i] + "", path, 6,6, arcLineFontPaint);				//沿着这条斜线画“距离信息”
                path.reset();															//重置路径
            }
        }
    }
    
   

    /**
     *     
     * 用户在地图上打标记
     * @param x
     * @param y
     */
    private void drawMarke(Canvas canvas, MapMarkeVo vo, Bitmap marketBitmap){

    	float x,y;
    	x = vo.getX();
    	y = vo.getY();
    	
    	x = x - marketBitmap.getWidth()/2;
    	y = y - marketBitmap.getHeight();
    	
        //到人的距离
        String distance =  mapModel.getPersonToMarke(vo,RountContexts.shortDistanceIndex);
        marketPaint.setColor(this.getResources().getColor(R.color.white));
        canvas.drawText(String.valueOf(distance) , x, y - 10, marketPaint);
        
        canvas.drawBitmap(marketBitmap, x, y, marketPaint);
    }
    
     public interface MapViewInterface{
        public void onclick();
    }
    
    private String[] distances;

    /**
     * 绘制当前用户点击的点
     * @param canvas
     */
    public void drawTouch(Canvas canvas) {
    	if (mapModel.getFuncButtonState() == FuncButtonState.cursor && mapModel.getTouchPoint().x > 0) {
    		Point crossoverPointA;// 点击点到果岭的点所在的直线与点击的点的圆的交点
    	    Point crossoverPointB;// 点击点到Tee台的点所在的直线与点击的点的圆的交点
    		Point touchPoint = mapModel.getTouchPoint();
//    		果岭距离
    		if (mapModel.getGreenPointArray().size()==1) {
					Point p = mapModel.getGreenPointArray().get(0);
					crossoverPointA = calculateIntersectionPoint(p, touchPoint);
					crossoverPointB = calculateIntersectionPoint(mapModel.getPersonPoint(), touchPoint);
					
					distances = mapModel.getDistanceTouchPointToGreenAndPerson(p,index);
					
					Point pointF=getMidpoint(p,mapModel.getTouchPoint());
					
					if (Integer.valueOf(distances[0]).intValue()>10) {
						drawLineWithShadow(canvas, p.x, p.y, crossoverPointA.x,crossoverPointA.y);
						canvas.drawBitmap(cursorBmp, mapModel.getTouchPoint().x- cursorBmp.getWidth() / 2, mapModel.getTouchPoint().y- cursorBmp.getHeight() / 2, defaultPaint);
		    			drawTextWithShadow(canvas, distances[0], pointF);	
						if (isTouchingCursor || Integer.valueOf(distances[1]).intValue()>1000) {
							drawTextWithShadow(canvas, distances[1],new Point(mapModel.getTouchPoint().x+60.0f,mapModel.getTouchPoint().y));
						}else {
							drawTextWithShadow(canvas, distances[1],getMidpoint(mapModel.getTouchPoint(), mapModel.getPersonPoint()));
						}
					}else {
	    				canvas.drawBitmap(cursorBmp, p.x-cursorBmp.getWidth() / 2 , p.y-cursorBmp.getHeight() / 2, defaultPaint);
	    				crossoverPointB = calculateIntersectionPoint(mapModel.getPersonPoint(), p);
	    				
					}
	    			drawLineWithShadow(canvas, mapModel.getPersonPoint().x,mapModel.getPersonPoint().y, crossoverPointB.x,crossoverPointB.y);
	    		}else if (mapModel.getGreenPointArray().size()==2) {
	    			distances = mapModel.getDistanceTouchPointToGreenAndPerson(mapModel.getGreenPointArray().get(0),index);
	    			int distance1 = Integer.valueOf(distances[0]).intValue();//触摸点到果岭一距离
	    			int distance2 = Integer.valueOf(mapModel.getDistanceTouchPointToGreenAndPerson(mapModel.getGreenPointArray().get(1),index)[0]).intValue();//触摸点到果岭二距离
	    			Point a = mapModel.getGreenPointArray().get(0);//果岭一
	    			Point b = mapModel.getGreenPointArray().get(1);//果岭二
	    			if (distance1>=10 && distance2>=10) {
//	    				distances = navigationMapModel.getDistanceToGreenAndMe(navigationMapModel.getPointGreens().get(0),index);
		    			
//	    				画果岭一到人的距离及线
	    				crossoverPointA = calculateIntersectionPoint(a, mapModel.getTouchPoint());
	    				drawLineWithShadow(canvas, a.x, a.y, crossoverPointA.x,crossoverPointA.y);
	    				Point pointA=getMidpoint(getMidpoint(a,touchPoint),a);
	    				drawTextWithShadow(canvas, distances[0]+"", pointA);	
//	    				画果岭二到人的距离及线		
	    				crossoverPointA = calculateIntersectionPoint(b, mapModel.getTouchPoint());
	    				drawLineWithShadow(canvas, b.x, b.y, crossoverPointA.x,crossoverPointA.y);
	    				Point pointB=getMidpoint(getMidpoint(b,touchPoint),touchPoint);
//	    				Point point2=new Point(pointB.x-index*60,pointB.y);
	    				drawTextWithShadow(canvas, distances[1]+"", pointB);
//	    				画触摸点
        				canvas.drawBitmap(cursorBmp, mapModel.getTouchPoint().x- cursorBmp.getWidth() / 2, mapModel.getTouchPoint().y- cursorBmp.getHeight() / 2, defaultPaint);
//        				画人到触摸点的线 及距离
        				crossoverPointB = calculateIntersectionPoint(mapModel.getPersonPoint(), mapModel.getTouchPoint());
        				drawLineWithShadow(canvas, mapModel.getPersonPoint().x,mapModel.getPersonPoint().y, crossoverPointB.x,crossoverPointB.y);
						if (isTouchingCursor  || Integer.valueOf(distances[1]).intValue()>1000) {
							drawTextWithShadow(canvas, distances[1],new Point(touchPoint.x+60.0f,touchPoint.y));
						}else {
							drawTextWithShadow(canvas, distances[1],getMidpoint(touchPoint, mapModel.getPersonPoint()));
						}
	    				
					}else if (distance1<10 && distance2>=10) {
						drawGreenShadow(canvas, a);
					}else if (distance1>=10 && distance2<10) {
						drawGreenShadow(canvas, b);
					}else if (distance1<10 && distance2<10) {
						if (distance1>=distance2) {
							drawGreenShadow(canvas, b);
						}else {
							drawGreenShadow(canvas, a);
						}
				} 
			}
		}
    }

    private void drawGreenShadow(Canvas canvas,Point p){
    	canvas.drawBitmap(cursorBmp, p.x- cursorBmp.getWidth() / 2, p.y- cursorBmp.getHeight() / 2, defaultPaint);
		drawLineWithShadow(canvas, mapModel.getPersonPoint().x,mapModel.getPersonPoint().y,p.x,p.y);
    }
    /**
     * 绘制一条带阴影的线。先绘制一条比较宽的黑线，然后在之上绘制一条白线
     * @param canvas
     * @param startX 开始的X坐标
     * @param startY 开始的Y坐标
     * @param stopX 结束的X坐标
     * @param stopY 结束的Y坐标
     */
    private void drawLineWithShadow(Canvas canvas, float startX, float startY,
            float stopX, float stopY) {
        canvas.drawLine(startX, startY, stopX, stopY, cursorLinePaint);
    }

    /**
     * 绘制一条带阴影的字符串，原理同drawLineWithShadow
     * @param canvas
     * @param text 要绘制的字符串
     * @param startPoint 开始的点
     */
    private void drawTextWithShadow(Canvas canvas, String text, Point startPoint) {
//        canvas.drawText(text, startPoint.x, startPoint.y, mPaint3);
        canvas.drawText(text, startPoint.x, startPoint.y, cursorFontPaint);
    }
    
    

    private Point tmpPoint;

    /**
     * 计算两点所在直线的中点
     * @param a 点A
     * @param b 点B
     * @return 点A与点B连线的中点
     */
    private Point getMidpoint(Point a, Point b) {
        tmpPoint = new Point();
        if (a.x > b.x) {
            tmpPoint.x = a.x - (a.x - b.x) / 2;
        } else {
            tmpPoint.x = a.x + (b.x - a.x) / 2;
        }

        if (a.y > b.y) {
            tmpPoint.y = a.y - (a.y - b.y) / 2;
        } else {
            tmpPoint.y = a.y + (b.y - a.y) / 2;
        }

        return tmpPoint;
    }

    /**
     * 计算两个点A与 以点B为圆心，半径为RADIUS的圆的交点
     * 
     * @param A
     *            点A
     * @param B
     *            点B
     * @return 点A与 以点B为圆心，半径为RADIUS的圆的交点
     */
    private Point calculateIntersectionPoint(Point A, Point B) {
        tmpPoint = new Point();
        float a = 0;
        float b = 0;
        double angle = 0;
        if (A.y > B.y && A.x <= B.x) {
            angle = Math.atan((B.x - A.x) / (A.y - B.y));
            a = (float) (Math.sin(angle) * RADIUS);
            b = (float) (Math.cos(angle) * RADIUS);
            tmpPoint.x = B.x - a;
            tmpPoint.y = B.y + b;
        } else if (A.y >= B.y && A.x > B.x) {
            angle = Math.atan((A.y - B.y) / (A.x - B.x));
            a = (float) (Math.cos(angle) * RADIUS);
            b = (float) (Math.sin(angle) * RADIUS);
            tmpPoint.x = B.x + a;
            tmpPoint.y = B.y + b;
        } else if (B.y > A.y && B.x <= A.x) {
            angle = Math.atan((A.x - B.x) / (B.y - A.y));
            a = (float) (Math.sin(angle) * RADIUS);
            b = (float) (Math.cos(angle) * RADIUS);
            tmpPoint.x = B.x + a;
            tmpPoint.y = B.y - b;
        } else if (B.y >= A.y && B.x > A.x) {
            angle = Math.atan((B.y - A.y) / (B.x - A.x));
            a = (float) (Math.cos(angle) * RADIUS);
            b = (float) (Math.sin(angle) * RADIUS);
            tmpPoint.x = B.x - a;
            tmpPoint.y = B.y - b;
        }
        return tmpPoint;
    }
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int measuredHeight = measureHeight(heightMeasureSpec);
        int measuredWidth = measureWidth(widthMeasureSpec);
        setMeasuredDimension(measuredWidth, measuredHeight);
    }

    private int measureHeight(int measureSpec) {
        int specMode = MeasureSpec.getMode(measureSpec);
        int specSize = MeasureSpec.getSize(measureSpec);

        int result = 420;

        if (specMode == MeasureSpec.AT_MOST) {
            result = specSize;
        } else if (specMode == MeasureSpec.EXACTLY) {
            result = specSize;
        }
        return result;
    }

    private int measureWidth(int measureSpec) {
        int specMode = MeasureSpec.getMode(measureSpec);
        int specSize = MeasureSpec.getSize(measureSpec);

        int result = 320;

        if (specMode == MeasureSpec.AT_MOST) {
            result = specSize;
        } else if (specMode == MeasureSpec.EXACTLY) {
            result = specSize;
        }
        return result;
    }

    
    /**
     * true 触摸人
     */
    private boolean isTouchMe = false;
//    private boolean isTouchCursor = false;
    private boolean isTouchingCursor = false;
//    private boolean isTouchMark = false;
    
    
    private boolean checkTouchMe(MotionEvent event)
    {
    	float x = event.getX();
    	float y = event.getY();
    	
        if (    (x< mapModel.getPersonPoint().x+image_offset) && 
        		(x>mapModel.getPersonPoint().x-image_offset) && 
        		(y>mapModel.getPersonPoint().y-image_offset) && 
        		(y<mapModel.getPersonPoint().y+image_offset) &&
        		!isPersonPosition() ) {
    		return true;
    	}
        return false;
    }
    public boolean checkMarkMoveValid(float vx, float vy)
    {
    	return checkMoveValid(vx, vy - (float)touch_offset_Y);
    }
    
    private boolean checkMoveValid(float vx, float vy)
    {
    	Log.e(TAG, util.getViewWidth()+","+util.getViewHeight());
    	float x = vx;
    	float y = vy;
		if (x < image_offset  ||
				y < image_offset || 
				x > (util.getViewWidth() - image_offset) ||	
				y > (util.getViewHeight() - image_offset)  
				) {
			/*check the outside of valid rectangle for view*/
			return false;
		}
		int buttonGroupWidth = mapModel.getButtonGroupView().getMeasuredWidth();
		int buttonGroupHeight = mapModel.getButtonGroupView().getMeasuredHeight();
		if((x < buttonGroupWidth+image_offset && 
				y >buttonGroupHeight + buttonY) ){
			/*check the inside of three function button*/
			return false;
		}
		Log.e(TAG, buttonGroupWidth+"-"+buttonGroupHeight);
		
		return true;
    }
    public boolean onActionDown(MotionEvent event)
    {
    	/*check pressed me or not*/
        isTouchMe = checkTouchMe(event);
        if(isTouchMe){
        	return true;
        }

        float x, y;
        x = event.getX();
        y = event.getY();
        int buttonGroupWidth = mapModel.getButtonGroupView().getMeasuredWidth();
		int buttonGroupHeight = mapModel.getButtonGroupView().getMeasuredHeight();
        /*check function button*/
        if( y < this.getMeasuredHeight() && 
        		y > this.getMeasuredHeight()- buttonGroupHeight - buttonY*2 &&        		 
        		 x < buttonGroupWidth+image_offset){
        		return false;      
        }
        isTouchingCursor = false;
        List<MapMarkeVo> voList = mapModel.getMapMarkeVoList();
        switch(mapModel.getFuncButtonState()){
        	case cursor:
        		mapModel.setTouchPoint(x, y	- touch_offset_Y);
//        		isTouchCursor = true;
        		isTouchingCursor = true;
        		break;
        	case addMark:
        		if (voList==null) {
					break;
				}
                /*check whether pressed mark or not*/
           		for (MapMarkeVo mapMarkeVo : voList) {
           			if(x > mapMarkeVo.getX() - image_offset && x < mapMarkeVo.getX() + image_offset && y > mapMarkeVo.getY() - image_offset && y < mapMarkeVo.getY() + image_offset){
           				voList.remove(mapMarkeVo);
           				mapMarkeVo.setX(x);
           				mapMarkeVo.setY(y-touch_offset_Y);
           				mapMarkeVo.setState(MapMarkeVo.STATE_4);
           				
           				this.movingMarkeVo = mapMarkeVo;
//           				isTouchMark = true;
           				return true;
           			}
           		}
           		
           		/*add new mark*/
    			if(voList.size() >= Contexts.MARGEK_COUNT){
    				return false;
    			}
    			
    			MapMarkeVo vo = new MapMarkeVo();
    			vo.setState(MapMarkeVo.STATE_4);
    			vo.setX(500);
    			vo.setY(800-touch_offset_Y);
//    			vo.setX(x);
//    			vo.setY(y-touch_offset_Y);
    			
//   				isTouchMark = true;
    			this.movingMarkeVo = vo;
    	        return true;
        	case removeMark:
        		if (voList==null) {
					break;
				}
 				for(int n=0 ;n<voList.size();n++) {
 					MapMarkeVo mapMarkeVo =voList.get(n);			
 					if(x > mapMarkeVo.getX() - image_offset 
 							&& x < mapMarkeVo.getX() + image_offset 
 							&& y > mapMarkeVo.getY() - image_offset 
 							&& y < mapMarkeVo.getY() + image_offset + markBitmap.getHeight()){
 						voList.remove(n);
 						return true;
 					}					
 				}
        		break;
        	default:
        		break;
        }
        return true;
    }
    

    public boolean onActionMove(MotionEvent event)
    {
        if (mapModel != null ){
        	if (isTouchMe) {
//        		double personLonLat[] = util.getLonLatByTouch(event.getX(), event.getY());
        		mapModel.setPersonPoint(event.getX(), event.getY());
        		handler.sendEmptyMessage(MapActivity.WHAT_LOCATION_REFRESH);
        		return true;
        	}
        }
    	
    	switch(mapModel.getFuncButtonState()){
    	case cursor:
            if(!checkMoveValid(event.getX(), event.getY())){
            	return false;
            }
            mapModel.setTouchPoint(event.getX(), event.getY()
					- touch_offset_Y);
			return true;
    	case addMark:
//    		if(isTouchMark){
                if(!checkMarkMoveValid(event.getX(), event.getY())){
                	return false;
                }
                if (movingMarkeVo!=null) {
                	this.movingMarkeVo.setX(event.getX());
                	this.movingMarkeVo.setY(event.getY() - touch_offset_Y);
                	return true;
				}
//    		}
    		break;
    	case removeMark:
    		
    		break;
    	default:
    		break;
    	}
    	
    	return false;
    }

	 public boolean onActionUp(MotionEvent event)
	    {
	    	isTouchingCursor = false;
	    	if(isTouchMe){
	    		isTouchMe = false;
	    		return true;
	    	}
	    	
	    	if(movingMarkeVo != null && mapModel.getFuncButtonState() == FuncButtonState.addMark){
	    		List<MapMarkeVo> voList = mapModel.getMapMarkeVoList();
				
	    		movingMarkeVo.setState(MapMarkeVo.STATE_1);
	    		
	    		/*check the mark position alidation*/
	    		if(movingMarkeVo.getY() < 0){
	    			movingMarkeVo.setY(markBitmap.getHeight());
	    		}
	    		movingMarkeVo.setHoleId(mapModel.getCurrentHoleId());
	    		voList.add(movingMarkeVo);
//	    		mapModel.setMapMarkeVoList(voList);
	    		movingMarkeVo = null;
	    		
	    		return true;
	    	}
	    	
	    	return false;
	    }
}
