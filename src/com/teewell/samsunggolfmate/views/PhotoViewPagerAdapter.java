package com.teewell.samsunggolfmate.views;

import java.util.ArrayList;
import java.util.List;

import com.teewell.samsunggolfmate.utils.BitmapUtil;
import com.teewell.samsunggolfmate.utils.ScreenUtil;
import com.teewell.samsunggolfmate.views.photoview.PhotoView;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;

public class PhotoViewPagerAdapter extends PagerAdapter {
	private ArrayList<String> photoPathList;
	private Context context;
	private List<Bitmap> list = new ArrayList<Bitmap>();
	public PhotoViewPagerAdapter(Context context,ArrayList<String> photoPathList) {
		this.context = context;
		this.photoPathList = photoPathList;
	}
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return photoPathList.size();
	}

	public List<Bitmap> getList() {
		return list;
	}
	public void setList(List<Bitmap> list) {
		this.list = list;
	}
	private Bitmap getBitmap(int position){
		if (position>=list.size()) {
			return null;
		}
		return list.get(position);
	}
	@Override
	public boolean isViewFromObject(View arg0, Object arg1) {
		// TODO Auto-generated method stub
		return arg0 == arg1;
	}
	@Override
	public View instantiateItem(ViewGroup container, int position) {
		PhotoView photoView = new PhotoView(container.getContext());
		Bitmap bitmap = getBitmap(position);
		if (bitmap == null) {
			bitmap = 
					BitmapUtil.getBitmapByScreenWidthOrHeight(photoPathList.get(position), ScreenUtil.getScreenWidth(context), ScreenUtil.getScreenHeight(context)-80);	
			list.add(bitmap);
		}
		photoView.setImageBitmap(bitmap);

		// Now just add PhotoView to ViewPager and return it
		container.addView(photoView, LayoutParams.MATCH_PARENT,
				LayoutParams.MATCH_PARENT);

		return photoView;
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		container.removeView((View) object);
	}
}
