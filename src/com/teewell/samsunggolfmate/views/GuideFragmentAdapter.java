package com.teewell.samsunggolfmate.views;

import java.util.ArrayList;

import com.teewell.samsunggolfmate.beans.GuideBean;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.ViewGroup;

public class GuideFragmentAdapter extends FragmentStatePagerAdapter {
	private ArrayList<GuideBean> guideBeanList;
	public ArrayList<Fragment> mFragments = new ArrayList<Fragment>();;
	private int index;

	public GuideFragmentAdapter(FragmentActivity activity,ArrayList<GuideBean> guideBeanList) {
		super(activity.getSupportFragmentManager());
		this.guideBeanList = guideBeanList;
	}



	public void Clear() {
		mFragments.clear();
	}

	public void addTab(Fragment fragment) {
		mFragments.add(fragment);
		notifyDataSetChanged();
	}
	public void addFragment() {
		// TODO Auto-generated method stub
		for (int i = 0; i < guideBeanList.size(); i++) {
			addTab(new GuideFragment(guideBeanList, i));
		}
	}

	@Override
	public Fragment getItem(int arg0) {
		return mFragments.get(arg0);
	}

	@Override
	public int getCount() {
		return mFragments.size();
	}

	@Override
	public int getItemPosition(Object object) {
		return POSITION_NONE;
	}

	//

	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		// TODO Auto-generated method stub
		super.destroyItem(container, position, object);
	}
}
