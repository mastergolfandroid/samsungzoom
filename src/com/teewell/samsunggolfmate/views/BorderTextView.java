package com.teewell.samsunggolfmate.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.widget.TextView;



public class BorderTextView extends TextView{
	
	private int color;
	@Override
	protected void onDraw(Canvas canvas)
	{
		super.onDraw(canvas);
		Paint paint = new Paint();
		paint.setColor(android.graphics.Color.BLACK);
		paint.setStrokeWidth(0.2f);
		drawLine(canvas, paint);
		
	}

	public BorderTextView(Context context, AttributeSet attrs){
		super(context, attrs);
	}
	
	
	public BorderTextView(Context context, int color){
		super(context);
		this.color=color;
	}
	
	
	private void drawLine(Canvas canvas,Paint paint ){
		switch (color){
		case 1:
			canvas.drawLine(0, 0, 0, this.getHeight(), paint);
			canvas.drawLine(this.getWidth() - 1, 0, this.getWidth() - 1,this.getHeight() , paint);
			break;
		case 2:
			
			canvas.drawLine(0, 0, 0, this.getHeight(), paint);
			canvas.drawLine(this.getWidth() - 1, 0, this.getWidth() - 1,this.getHeight() , paint);
			canvas.drawLine(0, this.getHeight() - 1, this.getWidth() - 1,this.getHeight() - 1, paint);	
			break;
		default:
			canvas.drawLine(0, 0, this.getWidth() - 1, 0, paint);
			canvas.drawLine(0, 0, 0, this.getHeight() - 1, paint);
			canvas.drawLine(this.getWidth() - 1, 0, this.getWidth() - 1,this.getHeight() - 1, paint);
			canvas.drawLine(0, this.getHeight() - 1, this.getWidth() - 1,this.getHeight() - 1, paint);		
			break;
		}
		
	}

}
