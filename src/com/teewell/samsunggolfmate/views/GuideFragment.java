package com.teewell.samsunggolfmate.views;

import java.io.IOException;
import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.android.volley.Cache;
import com.teewell.samsunggolfmate.activities.R;
import com.teewell.samsunggolfmate.activities.TabHostActivity;
import com.teewell.samsunggolfmate.beans.GuideBean;
import com.teewell.samsunggolfmate.models.GuideModel;
import com.teewell.samsunggolfmate.utils.MyVolley;

@SuppressLint("ValidFragment")
public class GuideFragment extends Fragment implements OnClickListener {
	private ArrayList<GuideBean> guideBeanList;
	private int index;
	private Button btn_gohome;
	private Cache cache;

	public GuideFragment(ArrayList<GuideBean> guideBeanList, int index) {
		super();
		this.guideBeanList = guideBeanList;
		this.setIndex(index);
		cache = GuideModel.getInstance(this.getActivity()).getCache();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.guide, null);
		cache = MyVolley.getRequestQueue().getCache();
		Log.e("aaaaaaaaaaa",String.valueOf(cache.get(guideBeanList.get(index).getUrl())));
		if (cache.get(guideBeanList.get(index).getUrl())!=null) {
			((ImageView)view.findViewById(R.id.step)).setImageBitmap(BitmapFactory.decodeByteArray(cache.get(guideBeanList.get(index).getUrl()).data, 0, cache.get(guideBeanList.get(index).getUrl()).data.length));
		}else {
			try {
				((ImageView)view.findViewById(R.id.step)).setImageBitmap(BitmapFactory.decodeStream(getActivity().getAssets().open("guide/"+(index+1)+".png")));
				GuideModel.getInstance(this.getActivity()).downloadBitmap(guideBeanList.get(index).getUrl());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
//		((ImageView) view.findViewById(R.id.step))
//				.setImageBitmap(BitmapUtil.readBitMapByNative(Contexts.GUIDE_PATH+guideBeanList.get(index).getName()));
		view.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Log.e("onclick", v.toString());
//				此处点击事件
			}
		});
		btn_gohome = (Button) view.findViewById(R.id.btn_gohome);
		btn_gohome.setOnClickListener(this);
		btn_gohome.setText("开始使用");
		if (index < guideBeanList.size() - 1) {
			btn_gohome.setVisibility(View.GONE);
		} else {
			btn_gohome.setVisibility(View.VISIBLE);
		}
		return view;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	@Override
	public void onResume() {
		super.onResume();

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_gohome:
			Intent intent = new Intent(getActivity(), TabHostActivity.class);
			getActivity().startActivity(intent);
			getActivity().finish();
			break;

		default:
			break;
		}
	}

}
