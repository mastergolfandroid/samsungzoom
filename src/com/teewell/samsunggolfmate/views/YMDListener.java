package com.teewell.samsunggolfmate.views;
/**  
 * @Project com.vehiclesApp
 * @package com.vehiclesApp.views
 * @title YMDListener.java 
 * @Description TODO
 * @author teewell
 * @date Nov 25, 2012 6:01:58 PM
 * @Copyright Copyright(C) Nov 25, 2012
 * @version 1.0.0
 */
public interface YMDListener {

	public void refreshYMD(String YMDString);
}
