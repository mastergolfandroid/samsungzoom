package com.teewell.samsunggolfmate.datebase;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import com.teewell.samsunggolfmate.utils.SaxReadSqlxml;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

/**
 * ��ݿ������
 * @author chengmingyan
 *
 */
public class DataBaseUpdate {
	/**
	 * ��ȡxml�ļ������
	 * @param context
	 * @param pathName
	 * @return
	 */
	public InputStream getXMLInputStream(Context context,String pathName){
		InputStream is = null;
		try {
			is = context.getAssets().open(pathName);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return is;
	}
	
	/**
	 * ��Sax����xml����ݿ�
	 * @param db
	 * @param verson
	 * @param context
	 * @param pathName
	 */
	public void update(SQLiteDatabase db,int verson,Context context,String pathName){
		InputStream is = null;
		try {
			is = context.getAssets().open(pathName);
			List<String> sql = new SaxReadSqlxml().readXml(is, verson);
			if (sql.size()>0) {
				for (int i = 0; i < sql.size(); i++) {
					System.out.println("Sax++++"+sql.get(i));
					db.execSQL(sql.get(i));
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
