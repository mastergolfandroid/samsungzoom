package com.teewell.samsunggolfmate.beans;

public class GolfLinkManBean {
	private int id;
	private String name;
	private String emails;
	private int e_id;
	private String sms;
	private byte[] photo;
	private int count;
	private int _id;
	
	public int get_id() {
		return _id;
	}
	public void set_id(int id) {
		_id = id;
	}
	public String getSms() {
		return sms;
	}
	public void setSms(String sms) {
		this.sms = sms;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmails() {
		return emails;
	}
	public void setEmails(String emails) {
		this.emails = emails;
	}
	public int getE_id() {
		return e_id;
	}
	public void setE_id(int eId) {
		e_id = eId;
	}
	public byte[] getPhoto() {
		return photo;
	}
	public void setPhoto(byte[] photo) {
		this.photo = photo;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
}
