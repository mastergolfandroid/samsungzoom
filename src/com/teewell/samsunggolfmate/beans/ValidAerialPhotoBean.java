package com.teewell.samsunggolfmate.beans;

import android.graphics.Bitmap;

public class ValidAerialPhotoBean {
	private int[] validArea;//
	private Bitmap bitmap;
	public int[] getValidArea() {
		return validArea;
	}
	public void setValidArea(int[] validArea) {
		this.validArea = validArea;
	}
	public Bitmap getBitmap() {
		return bitmap;
	}
	public void setBitmap(Bitmap bitmap) {
		this.bitmap = bitmap;
	}
	
}
