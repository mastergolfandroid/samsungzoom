package com.teewell.samsunggolfmate.beans;

import java.io.Serializable;

import org.json.JSONArray;
import org.json.JSONObject;

@SuppressWarnings("serial")
public class GeneralScoreBean implements Serializable{
	
    private int courseID;            
    private String course_Name;      
    private int mySelfScore;         
    private String startTee;
    private String startHole; 
    private String matchType = "比杆赛";//分为比杆赛（按72杆标准的）和比洞赛（比较没动的+ — 情况）
    private String abbreviation;
    private int scores[] = new int[18];
    private int pushRods[] = new int[18];
    private int pars[];
    
	public String getStartTee() {
		return startTee;
	}
	public void setStartTee(String startTee) {
		this.startTee = startTee;
	}
	
	public String getStartHole() {
		return startHole;
	}
	public void setStartHole(String startHole) {
		this.startHole = startHole;
	}
	public int getCourseID() {
		return courseID;
	}
	public void setCourseID(int courseID) {
		this.courseID = courseID;
	}
	public String getCourse_Name() {
		return course_Name;
	}
	public void setCourse_Name(String course_Name) {
		this.course_Name = course_Name;
	}
	public int getMySelfScore() {
		return mySelfScore;
	}
	public void setMySelfScore(int mySelfScore) {
		this.mySelfScore = mySelfScore;
	}
	public String getMatchType() {
		return matchType;
	}
	public void setMatchType(String matchType) {
		this.matchType = matchType;
	}
	public String getAbbreviation() {
		return abbreviation;
	}
	public void setAbbreviation(String abbreviation) {
		this.abbreviation = abbreviation;
	}
	
	public int[] getScores() {
		return scores;
	}
	public void setScores(int[] scores) {
		this.scores = scores;
	}
	public int[] getPushRods() {
		return pushRods;
	}
	public void setPushRods(int[] pushRods) {
		this.pushRods = pushRods;
	}
	public int[] getPars() {
		return pars;
	}
	public void setPars(int[] pars) {
		this.pars = pars;
	}
	public static GeneralScoreBean jsonGeneralScoreBean(JSONObject object) {
		GeneralScoreBean bean = new GeneralScoreBean();
		bean.setMatchType(object.optString("matchType"));
		bean.setAbbreviation(object.optString("abbreviation"));
		bean.setCourseID(object.optInt("courseID"));
		bean.setCourse_Name(object.optString("course_Name"));
		bean.setMySelfScore(object.optInt("mySelfScore"));
		bean.setStartHole(object.optString("startHole"));
		bean.setStartTee(object.optString("startTee"));
		JSONArray array = object.optJSONArray("parArray");
		int pars[] = new int[18];
		for (int i = 0; i < array.length(); i++) {
			pars[i] = array.optInt(i);
		}
		bean.setPars(pars);
		array = object.optJSONArray("scoreArray");
		int scores[] = new int[18];
		for (int i = 0; i < array.length(); i++) {
			scores[i] = array.optInt(i);
		}
		bean.setScores(scores);
		array = object.optJSONArray("pushArray");
		int pushRods[] = new int[18];
		for (int i = 0; i < array.length(); i++) {
			pushRods[i] = array.optInt(i);
		}
		bean.setPushRods(pushRods);
	
		return bean;
	} 
}
