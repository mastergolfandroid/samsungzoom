package com.teewell.samsunggolfmate.beans;

/**
 * @author chengmingyan
 *
 */
public class ProvinceBean {
    private String provinceName;//
    private int pos;
/**
     * @return the isCheck
     */

    public String getProvinceName() {
		return provinceName;
	}
	public void setProvinceName(String provinceName) {
		this.provinceName = provinceName;
	}
	/**
	 * @return the pos
	 */
	public int getPos() {
		return pos;
	}
	/**
	 * @param pos the pos to set
	 */
	public void setPos(int pos) {
		this.pos = pos;
	}
}
