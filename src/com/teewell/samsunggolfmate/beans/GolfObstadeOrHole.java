package com.teewell.samsunggolfmate.beans;

import java.io.Serializable;


public class GolfObstadeOrHole implements Serializable {
	/**
     * serialVersionUID
     */
    private static final long serialVersionUID = 1L;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public String getLen() {
		return len;
	}
	public void setLen(String len) {
		this.len = len;
	}

	private String name;
	private String len;
}