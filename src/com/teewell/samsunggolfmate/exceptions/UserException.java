package com.teewell.samsunggolfmate.exceptions;

/**
 * @description
 * @author 黄河
 * @title UserException.java   
 * @package com.mncloud.core.user.dao 
 * @date 2012-4-5 上午12:42:22  
 * @version 1.0.0.1  
 * Copyright: Copyright (c)  2012    
 */
public class UserException{

	/**
	 * 用户登录名已经存在，不能注册
	 */
	public static final int USERNAME_IS_EXIST=-10000001;
	/**
	 * 用户登录手机已经存在，并已经被激活使用，不能注册
	 */
	public static final int MOBILE_IS_EXIST=-10000002;
	/**
	 * 用户登录email已经存在，并且已经被激活，不能注册
	 */
	public static final int EMAIL_IS_EXIST=-10000003;
	/**
	 * 第三方登录信息已经存在，不能注册
	 */
	public static final int THIRD_IS_EXIST=-10000004;
	/**
	 * 缺少必要的注册信息(主要指除密码以外的登录数据)
	 */
	public static final int REGISTER_IS_NOT_EXIST=-10000005;
	/**
	 * 缺少登录用户名
	 */
	public static final int MISSING_LOGIN_NAME=-10000006;
	/**
	 * 缺少登录密码
	 */
	public static final int MISSING_PASSWORD=-10000007;
	/**
	 * 用户名不能是手机格式
	 */
	public static final int USERNAME_IS_MOBILE=-10000008;
	/**
	 * 用户名不能是邮箱格式
	 */
	public static final int USERNAME_IS_EMAIL=-10000009;
	/**
	 * 用户的密码错误
	 */
	public static final int USERNAME_PASSWORD_ERR=-10000010;
	/**
	 * 用户已经被禁用
	 */
	public static final int USER_IS_STOPPED=-10000011;
	/**
	 * 用户名不存在
	 */
	public static final int USERNAME_NOT_EXIST=-10000012;
	/**
	 * 用户登录手机号码未激活
	 */
	public static final int USER_MOBILE_IS_NOT_ACTIVATE=-10000013;
	/**
	 * 用户登录邮箱未激活
	 */
	public static final int USER_EMAIL_IS_NOT_ACTIVATE=-10000014;
	/**
	 * 用户已经被删除
	 */
	public static final int USER_IS_DELETED=-10000015;
	/**
	 * 用户手机号码格式错误
	 */
	public static final int USER_MOBILE_ERR=-10000016;
	/**
	 * 用户邮件格式错误
	 */
	public static final int USER_EMAIL_ERR=-10000017;
	/**
	 * 注册用户数据到数据库出错
	 */
	public static final int REGISTER_DATABASE_ERR=-10000018;
	/**
	 * 邮箱地址不存在
	 */
	public static final int EMAIL_NOT_EXIST=-10000019;
	/**
	 * 手机号码不存在
	 */
	public static final int MOBILE_NUMBER_NOT_EXIST=-10000020;
	/**
	 * 用户ID不存在
	 */
	public static final int USER_ID_NOT_EXIST=-10000021;
	/**
	 * 第三方不存在
	 */
	public static final int THIRD_NOT_EXIST=-10000022;
	/**
	 * 用户被其他地方登录
	 */
	public static final int USER_WHERE_LOGINED=-10000023;
	/**
	 * 用户已经离线,请重新登录
	 */
	public static final int USER_OFFLINE=-10000024;
	/**
	 * 用户客户端类型错误
	 */
	public static final int USER_CLIENT_TYPE_ERR=-10000025;
	/**
	 * 验证码错误
	 */
	public static final int USER_VALIDATE_CODE_ERR=-10000026;
	/**
	 * 系统缺少基本的角色关系
	 */
	public static final int MISSING_ROLE=-10000027;
	
	/**
	 * 两次输入的密码不相同
	 */
	public static final int PASSWORD_NOT_EQUAL=-10000028;
	/**
	 * 角色不存在
	 */
	public static final int ROLE_IS_NOT_EXIST=-10000029;
	
	/**
	 * 该acl对象不存在
	 */
	public static final int ACL_IS_NOT_EXIST=-10000030;
	/**
	 * 数据校验错误
	 */
	public static final int CLOUD_DATA_VERIFY_ERROR=-10000031;
	/**
	 * 找回密码方法错误
	 */
	public static final int LOST_PASSWORD_METHOD=-10000032;
	/**
	 * 凭证错误
	 */
	public static final int USER_CREDENTIAL_ERROR=-10000033;
	/**
	 * 用户不存在
	 */
	public static final int USER_NOT_EXIST=-10000034;
	/**
	 * 用户已经存在
	 */
	public static final int USER_IS_EXIST=-10000035;
	/**
	 * 验证码错误
	 */
	public static final int USER_VERIFYCODE_ERROR=-10000036;
	/**
	 * 验证码已经过期
	 */
	public static final int USER_VERIFYCODE_OVERDUE=-10000037;
	/**
	 * 验证码生成过于频繁
	 */
	public static final int USER_VERIFYCODE_FREQUENTLY=-10000038;
	/**
	 * 验证码数据校验错误
	 */
	public static final int USER_VERIFYCODE_ERROR1=-10000039;
	
	public static final long serialVersionUID = 6071946417772934221L;
	/**
	 * 角色为超级管理员，不能被删除
	 */
	public static final int ROLE_IS_ADMIN = -10000040;

}
